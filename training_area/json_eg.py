import json
import os

# JSON file for stats
if os.path.isfile('statsOfDatasets.json'):
    with open("statsOfDatasets.json", "r") as read_file:
        data = json.load(read_file)
else:
    data = {}




data[1234] = {
    'Average img size' : (1, 2),
    'Min image' : (3, 4),
    'Max image' : (5, 6)
}

with open('statsOfDatasets.json', 'w', encoding='utf-8') as write_file:
    json.dump(data, write_file, ensure_ascii=False, indent=4)