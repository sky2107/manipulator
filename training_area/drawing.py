import matplotlib.pyplot as plt
import numpy as np
def loadArrays():
    '''
        loadArrays
    '''
    return np.load('np_arrays\\val_loss.npy'),  np.load('np_arrays\\train_loss.npy')

vv, tt = loadArrays()
v_fct = vv
t_fct = tt
middle_fct = (t_fct + v_fct) / 2
plt.xlabel('Length of dataset / batchsize')
plt.ylabel('Losses of train and val')
plt.title('Compare both loss arrays with each other')
x_axis = [x for x in range(v_fct.size)]
idx = np.argwhere(np.diff(np.sign(t_fct - v_fct)) != 0).reshape(-1) + 0
plt.plot(x_axis, t_fct, 'g-')
plt.plot(x_axis, v_fct, 'y-')
# plt.plot(y_axis, middle_fct, 'r-')
# idx = np.argwhere(np.diff(np.sign(t_fct - v_fct))).flatten()
for i in range(len(idx)):
    print(len(idx))
    print((x_axis[idx[i]]+x_axis[idx[i]+1])/2)
    print((x_axis[idx[i]]))
    print(x_axis[idx[i]+1])
    
    # plt.plot((x_axis[idx[i]]+x_axis[idx[i]+1])/2.,
    #          (t_fct[idx[i]]+t_fct[idx[i]+1])/2., 'ro')
    plt.plot((x_axis[idx[i]]+x_axis[idx[i]+1])/2.,(t_fct[idx[i]]+t_fct[idx[i]+1]+v_fct[idx[i]]+v_fct[idx[i]+1])/4., 'ro')
# plt.plot(v_fct[idx], t_fct[idx], 'ro')
plt.show()
np.save('./array', v_fct)

