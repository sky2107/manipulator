import time
import torch
from tqdm import tqdm

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
pbar = tqdm(total=29)
for _ in range(29):
    time.sleep(.5)
    pbar.update(1)
print(device)