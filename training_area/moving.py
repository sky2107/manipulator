import os
import shutil
import random


src_fol = './data/screenshots_val/9/'
dest_fol = './data/screenshots/9/'
files = []
for i in range(50):
    files.append(random.choice([x for x in os.listdir(src_fol)
                                if os.path.isfile(os.path.join(src_fol, x)) and x not in files]))
    src = os.path.join(src_fol, files[i])
    dest = os.path.join(dest_fol, files[i])
    shutil.move(src, dest)
    
print(len(files))