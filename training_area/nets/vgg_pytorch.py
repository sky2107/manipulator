import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.models as models
from tqdm import tqdm

from model.transformerPYTorch import TransformerPYTorch
class AlexNet(nn.Module):

    def __init__(self, num_classes=10):
        self.init ='Kaiming'
        super(AlexNet, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(3, 96, kernel_size=11, stride=4, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(96, 256, kernel_size=5, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(256, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, kernel_size=3, padding=1),
            
        )
        self.classifier = nn.Sequential(
            nn.Dropout(p=.2),
            nn.Linear(256, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, 4096),
            nn.LogSoftmax(dim=1),
            nn.Linear(4096, num_classes),
        )

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), 256)
        x = self.classifier(x)
        return x
    
    def init_kaimingHE(self, m):
        '''
            all the weights plus bias zero
            no he
            The He initialization performance better than the Xavier initialization
            Kaiming only by leaky
            NORMALLY HE fct
            if 'bogiot' == method_of_init:
                method_for_w_to_init = 'he_uniform'
            elif 'hinton' == method_of_init:
                method_for_w_to_init = 'he_normal'
        '''
        if type(m) == nn.Linear:
            if self.init == 'Kaiming':
                nn.init.kaiming_uniform_(m.weight)
            elif self.init == 'Xavier':
                nn.init.xavier_uniform_(m.weight)
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif type(m) == nn.Conv2d:
            if self.init == 'Kaiming':
                nn.init.kaiming_uniform_(m.weight)
            elif self.init == 'Xavier':
                nn.init.xavier_uniform_(m.weight)
            if m.bias is not None:
                # nn.init.kaiming_uniform_(m.bias)
                nn.init.constant_(m.bias, 0)

   
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
pixel_w = 30
pixel_h = 30
channels = 3
path_of_data = '.\\data\screenshots'
batch_size = 32


loader = TransformerPYTorch()
data_loader = loader.dataTransformer(
    pixel_w, pixel_h, channels, path_of_data, batch_size)

image = loader.process_image(
    '.\\data\screenshots\\0\\0_0_ImageEnhance_Brightness_2.jpg', pixel_w, pixel_h)

# vgg16 = models.vgg16()
alexnet = AlexNet()
alexnet.apply(alexnet.init_kaimingHE)

for i, (name, layer) in enumerate(alexnet.named_modules()):
    print(layer)
    print(name)
    # if isinstance(layer, nn.ReLU):
    #     print(layer)
    #     print(name)
print('--------------------------------')
# layer = NewActivation()



criterion = torch.nn.CrossEntropyLoss()
optimizer = optim.Adam(alexnet.parameters(), lr=.001)
alexnet.train()
only_two_times = 0
for images, labels in tqdm(data_loader):
    # Move to device
    images, labels = images.to(device), labels.to(device)
    ###################################################################
    # Clear optimizers
    optimizer.zero_grad()
    # Forward pass
    output = alexnet.forward(images)
    # Loss
    loss = criterion(output, labels)
    # Calculate gradients (backpropogation)
    loss.backward()
    # Adjust parameters based on gradients
    optimizer.step()
    if only_two_times == 2:
        break

    only_two_times += 1

alexnet.eval()
with torch.no_grad(): 
    output = alexnet.forward(image)

    # Reverse the log function in our output
    output = torch.exp(output)

    print(output)
    # Get the top predicted class, and the output percentage for
    # that class
    probs, classes = output.topk(1, dim=1)
    print(probs.item(), classes.item())
