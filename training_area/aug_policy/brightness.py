from PIL import ImageEnhance, Image

img = Image.open('./airplane.jpg')
img_bright = ImageEnhance.Brightness(img)

img_bright.enhance(1.3).show()