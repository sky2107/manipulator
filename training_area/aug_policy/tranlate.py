from PIL import Image
import PIL.ImageOps
import glob

shift = 5
files = glob.glob('./../*.JPG') # Use *.* if you're sure all are images

for f in files:
    image = Image.open(f)
    inverted_image = PIL.ImageOps.invert(image)

    out = f[:f.rfind('.')]
    inverted_image.save('%s-n.JPG'%out)

    # Shift the image 5 pixels
    width, height = image.size
    shifted_image = Image.new("RGB", (width+shift, height))
    shifted_image.paste(image, (shift, 0))
    shifted_image.save('%s-shifted.JPG' % out)