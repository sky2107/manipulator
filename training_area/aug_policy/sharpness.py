from PIL import ImageEnhance, Image


img = Image.open('./airplane.jpg')
enhancer = ImageEnhance.Sharpness(img)

for i in range(8):
    factor = i / 4.0
    enhancer.enhance(3).show("Sharpness %f" % 3)
    exit()
