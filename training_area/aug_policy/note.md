# Pillow Ops

> [ImageEnhance](https://pillow.readthedocs.io/en/3.0.x/reference/ImageEnhance.html) <br>
> [code_eg](https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/ImageOps.html) <br>
> [filter](https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/ImageFilter.html) <br>
> Sharpness, Brightness, Contrast <br>
> [paper](https://www.researchgate.net/publication/322355257_Data_Augmentation_by_Pairing_Samples_for_Images_Classification) <br>
> [text](https://link)