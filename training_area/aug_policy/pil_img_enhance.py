from PIL import ImageEnhance, Image, ImageOps

img = Image.open('./airplane.jpg')
img_bright = ImageEnhance.Brightness(img)
# f 0 - 1
n_bright = 1
# img_bright.enhance(n_bright).show()

sharper = ImageEnhance.Sharpness(img)
# f  0 - 1 -2
n_sharp = 1
# sharper.enhance(n_sharp).show()

img_color = ImageEnhance.Color(img)
# f 0 - 1
n_color = 2
img_color.enhance(n_color).show()

# applying autocontrast method
autocontrast = ImageOps.autocontrast(img, cutoff = 2, ignore = 2)
# im2 = ImageOps.autocontrast(im1, cutoff = 5, ignore = 5)

autocontrast.show()