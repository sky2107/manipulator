import glob
import json
import os
import random
import shutil
import sys
import timeit
from time import time
from shutil import copyfile, rmtree

from tqdm import tqdm

import numpy as np
from PIL import Image, ImageChops, ImageEnhance, ImageFilter, ImageOps
from PIL.ImageFilter import (CONTOUR, DETAIL, EDGE_ENHANCE, EDGE_ENHANCE_MORE,
                             EMBOSS, FIND_EDGES, SHARPEN, SMOOTH, SMOOTH_MORE,
                             MaxFilter, MedianFilter, MinFilter, ModeFilter,
                             RankFilter)


class PrincipleDataAugmentation:
    '''
        pydoc with PIL https://pillow.readthedocs.io/en/3.0.x/index.html
        [paper](https://www.researchgate.net/publication/322355257_Data_Augmentation_by_Pairing_Samples_for_Images_Classification)
    '''

    def __init__(self, path_of_images: str, still_to_do: bool = True):
        self.ENDING = '.jpg'
        self.path_of_images = path_of_images
        self.all_labels = self.getAllLabels(self.path_of_images)
        self.label_count = len(self.all_labels)
        self.allImages = self.getAllImages(self.all_labels)
        self.filter_folder = './data/filterfct'
        self.enhance_folder = './data/enhancefct'
        if still_to_do:
            for label in self.all_labels:
                self.filterAllImg(label, self.allImages[label])
            for label in self.all_labels:
                self.enhanceAllImages(label, self.allImages[label])

    def getAllLabels(self, path: str) -> list:
        '''
            all labels for classification
            natural_images => ['airplane', 'car', 'cat', 'dog', 'flower', 'fruit', 'motorbike', 'person']
            screenshots => ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        '''
        dir_listi = []
        for root, dirs, files in os.walk(path, topdown=False):
            if len(dirs) > 0:
                dir_listi = (dirs)

        return dir_listi

    def getAllImages(self, path_of_labels: list, ending: str = '.jpg') -> dict:
        '''
            return all images path via dict
        '''
        allImages = {}
        list_of_files = []
        for label_path in path_of_labels:
            for file in os.listdir(self.path_of_images + label_path):
                if file.endswith(ending):
                    list_of_files.append(os.path.join(
                        self.path_of_images + label_path, file))
            allImages[label_path] = list_of_files
            list_of_files = []
        return allImages

    def filterAllImg(self, label_path: list, images_of_label: list) -> None:
        '''
            filterAllImg
            before creating folder structure
        '''
        if not os.path.isdir(self.filter_folder):
            os.mkdir(self.filter_folder)
        full_path = os.path.join(self.filter_folder, label_path)
        if not os.path.isdir(full_path):
            os.mkdir(full_path)
        # print(type(images_of_label))
        for path_img in images_of_label:
            img = self.openImage(path_img)
            filename = img.filename.split(self.path_of_images)[
                1].split('/')[1]
            self.allFilterFunctions(img, filename, full_path)

    def allFilterFunctions(self, img, img_name, label_path) -> None:
        '''
            Active CONTOUR, DETAIL, EDGE_ENHANCE, EDGE_ENHANCE_MORE,
            EMBOSS, FIND_EDGES, SMOOTH, SMOOTH_MORE, SHARPEN,
            PER DEFAULT NOT ACTIVE
            RankFilter, MedianFilter, MinFilter, MaxFilter, ModeFilter
            example https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/ImageFilter.html
        '''
        KERNEL = self.getKernel()
        if True:
            for f in (
                    CONTOUR, DETAIL, EDGE_ENHANCE, EDGE_ENHANCE_MORE,
                    EMBOSS, FIND_EDGES, SMOOTH, SMOOTH_MORE, SHARPEN, KERNEL):
                new_path = os.path.join(
                    label_path, img_name.split(self.ENDING)[0])
                img.filter(f).save(
                    new_path + '_{}_.jpg'.format(f.name.replace(" ", "-")))
        if True:
            for f in (RankFilter, MedianFilter, MinFilter, MaxFilter, ModeFilter):
                new_path = os.path.join(
                    label_path, img_name.split(self.ENDING)[0])
                if f is RankFilter:
                    img.filter(f(size=9, rank=2)).save(
                        new_path + '_{}_.jpg'.format(f.name.replace(" ", "-")))
                else:
                    img.filter(f(size=9)).save(
                        new_path + '_{}_.jpg'.format(f.name.replace(" ", "-")))

    def getKernel(self, flag: bool = True):
        '''
            one kind of emboss FLAG True default else
            Gaussian blur 5 x 5 (approximation)
        '''
        if flag:
            km = (
                -2, -1,  0,
                -1,  1,  1,
                0,  1,  2
            )
            k = ImageFilter.Kernel(
                size=(3, 3),
                kernel=km,
                scale=sum(km),  # default
                offset=0  # default
            )
        else:
            km = np.array((
                (1, 4, 6, 4, 1),
                (4, 16, 24, 16, 4),
                (6, 24, 36, 24, 6),
                (4, 16, 24, 16, 4),
                (1, 4, 6, 4, 1),
            )) / 256.
            k = ImageFilter.Kernel(
                size=km.shape,
                kernel=km.flatten(),
                scale=np.sum(km),  # default
                offset=0  # default
            )
        return k

    def enhanceAllImages(self, label_path: list, images_of_label: list):
        '''
            enhanceAllImages
            before creating folder structure
        '''
        if not os.path.isdir(self.enhance_folder):
            os.mkdir(self.enhance_folder)
        full_path = os.path.join(self.enhance_folder, label_path)
        if not os.path.isdir(full_path):
            os.mkdir(full_path)
        for path_img in images_of_label:
            img = self.openImage(path_img)
            filename = img.filename.split(self.path_of_images)[
                1].split('/')[1]
            self.allEnhanceFct(img, filename, full_path)

    def allEnhanceFct(self, img, img_name, label_path):
        '''
            Color, Contrast, Brightness, Sharpness PLUS
            Invert, convert with matrix rgb2xyz, equalize
            [ImageEnhance](https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/ImageEnhance.html)
            for invert ImageChops and rgb2xyz
            [invert](https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/ImageChops.html#invert)
            [convert mat](https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/Image__class_Image.html)
        '''
        new_path = os.path.join(
            label_path, img_name.split(self.ENDING)[0])
        color = ImageEnhance.Color(img)
        contrast = ImageEnhance.Contrast(img)
        brightness = ImageEnhance.Brightness(img)
        sharperness = ImageEnhance.Sharpness(img)
        for scale in range(0, 5):
            color.enhance(scale / 4).save(
                new_path + '_{}_.jpg'.format(str(scale) + '_ImageEnhance_Color'))
            contrast.enhance(scale / 4).save(
                new_path + '_{}_.jpg'.format(str(scale) + '_ImageEnhance_Contrast'))
            brightness.enhance(scale / 4).save(
                new_path + '_{}_.jpg'.format(str(scale) + '_ImageEnhance_Brightness'))
            sharperness.enhance(scale * 2 / 4).save(
                new_path + '_{}_.jpg'.format(str(scale * 2) + '_ImageEnhance_Brightness'))
        # Invert, convert with matrix rgb2xyz, equalize TODO bad design
        invert = ImageChops.invert(img)
        invert.save(new_path + '_{}_.jpg'.format('Invert'))
        # rgb2xyz
        mat = (
            0.412453, 0.357580, 0.180423, 0,
            0.212671, 0.715160, 0.072169, 0,
            0.019334, 0.119193, 0.950227, 0)
        img.convert("RGB", mat).save(
            new_path + '_{}_.jpg'.format('Convert_with_matrix'))
        # applying equalize method
        equalize = ImageOps.equalize(img, mask=None)
        equalize.save(new_path + '_{}_.jpg'.format('equalize'))

    def openImage(self, path_img):
        return Image.open(path_img)

    def colorFct(self):
        '''
            TODO
            https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/ImageColor.html
        '''
        pass

    def pairing(self):
        '''
            TODO
            https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/ImageChops.html#darker
        '''
        pass

    def moveFile(self, src, dest):
        '''
            movingfiles
        '''
        # both techniques are doing the same
        # os.rename(src, dest)
        shutil.move(src, dest)

    def walk_through_data(self, path: str = './data', ending: str = '.jpg') -> list:
        '''
            doc
        '''
        list = []
        for root, dirs, files in os.walk(path, topdown=False):
            for name in files:
                if name.endswith(ending):
                    list.append(os.path.join(root, name))
                    # list.append(name)
        return list

    def movingFilesToDestWithLabels(self, labels, dicti, final_destination):
        '''
            Moving
        '''
        for label in labels:
            for file in dicti[label]:
                # print(file)
                filename = file.split('/')[-1] # .split('.jpg')[0]
                dest = os.path.join(final_destination, label)
                self.moveFile(file, os.path.join(dest, filename))

    def cleanUpLabelStructure(self, folder):
        '''
            CLEAN UP and DELETE ALL FILES AND FOLDERS
        '''
        # removes also all files and subfolders
        if os.path.isdir(folder):
            rmtree(folder)
        else:
            print('No folder to delete')

if __name__ == "__main__":
    start = time()
    # img_handler = PrincipleDataAugmentation('./original/natural_images/')
    img_handler = PrincipleDataAugmentation('./data/natural_images/', False)

    filter_fol = img_handler.filter_folder
    enhance_fol = img_handler.enhance_folder

    if True:
        final_destination = './data/natural_images/'
        # final_destination = './data/screenshots/'

        dicti1 = {}
        dicti2 = {}

        labels = img_handler.getAllLabels(filter_fol)
        list_of_img = img_handler.walk_through_data(filter_fol)

        listi_of_files = []

        for label in tqdm(labels, desc='Moving files'):
            dicti1[label] = img_handler.walk_through_data(os.path.join(filter_fol, label))
            dicti2[label] = img_handler.walk_through_data(os.path.join(enhance_fol, label))

        img_handler.movingFilesToDestWithLabels(labels, dicti1, final_destination)
        img_handler.movingFilesToDestWithLabels(labels, dicti2, final_destination)

    clean_up = False

    if clean_up:
        img_handler.cleanUpLabelStructure(filter_fol)
        img_handler.cleanUpLabelStructure(enhance_fol)
    # print(labels)
    # print(list_of_img)
    print('Time')
    print(time() - start)
    