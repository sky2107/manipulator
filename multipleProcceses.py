import subprocess
from subprocess import Popen
from time import time
from tqdm import tqdm

print('Start')
'''
    for loops to train
    2 * 3 * epochs * 4 * 2 * input_size
    nets * bt * ep * lr * optim * input_size
    6 hyperparameters
'''
networks = ['AlexNet', 'LeNet'] # ['AlexNet', 
batch_sizes = [32, 128] # , 64, 128]
epochs = [3] # [1, 3, 5, 10]
lr = [0.001, 0.003] # , 0.003, 0.01, 0.03]
optim = ['Adam', 'SGD']
input_shape = [(240, 240, 3)]
program = 'public/controller.py '
all_trainings = []

for net in networks:
    cmd_net = '--cnn_name=' + net
    cmd_net += ' '
    for opti in optim:
        cmd_optim = '--optimizer=' + opti
        cmd_optim += ' '
        for bt in batch_sizes:
            cmd_bt = '--batch_size=' + str(bt)
            cmd_bt += ' '
            for l in lr:
                cmd_lr = '--lr=' + str(l)
                cmd_lr += ' '
                for in_size in input_shape:
                    cmd_input_shape = '--input_shape=(' + str(in_size[0]) + str(in_size[1]) + str(in_size[2]) + ')'
                    cmd_input_shape += ' '
                    for ep in epochs:
                        cmd_ep = '--epochs=' + str(ep)
                        all_trainings.append(
                            program + cmd_net + cmd_optim + cmd_bt + cmd_lr + cmd_input_shape + cmd_ep)

print('Over {0:d} trainings'.format(len(all_trainings)))
start = time()
index = 0
counter = 1
pbar_len = len(all_trainings)
progress_bar = tqdm(total=pbar_len)
for train in all_trainings:
    if counter <= 8:
        pass
    else:
        print('Begining of training ', counter)
        prog = all_trainings[index].split(' ')
        print(prog)
        Popen(['python', prog[0], prog[1], prog[2], prog[3],
            prog[4], prog[6]]).wait()
    if counter < len(all_trainings) + 1:
        print('Next training')
        print()
        # !
        progress_bar.update(1)
    counter += 1
    index += 1

print('-------------------------------------------------------------------------------------')
print('Finished time spent for this process with {0:d} training models was '.format(counter))
end = time() - start
print(end)
