# Experiment Screenshots

> It is necessary to generate up to 1000 images for each class. <br>
> This approach is a conclusion through the study in the ImageNet paper. \cite{ImageNet} <br>
> Cross validation will make no sense with this data set. To try to get the robostness of the model, <br>
> it will try to indentify images from the web with similar content. Because teh training accuracy or <br>
> training loss will not be good indicators. Another idea

    Parameters of images
    10
    10
    Average img size:  30 30
    Min image:  30 30
    Max image:  30 30
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

> Manipulation was done with the dynamically implemented classes for the data augmentation. <br>
> ImageNet is a dataset of over 15 million labeled high-resolution images belonging to roughly 22,000 <br>
> categories. The images were collected from the web and labeled by human labelers using Amazon’s Mechanical <br>
> Turk crowd-sourcing tool. Starting in 2010, as part of the Pascal Visual Object <br>
> Challenge, an annual competition called the ImageNet Large-Scale Visual Recognition Challenge <br>
> (ILSVRC) has been held. ILSVRC uses a subset of ImageNet with roughly 1000 images in each of <br>
> 1000 categories. In all, there are roughly 1.2 million training images, 50,000 validation images, and <br>
> 150,000 testing images.
