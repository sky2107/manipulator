# 

[ResNEt He Kaiming](https://github.com/KaimingHe/deep-residual-networks)

@article{He2015,
	author = {Kaiming He and Xiangyu Zhang and Shaoqing Ren and Jian Sun},
	title = {Deep Residual Learning for Image Recognition},
	journal = {arXiv preprint arXiv:1512.03385},
	year = {2015}
}

note more generalizeable good for beginners

AlexNet:

    ValueError: Negative dimension size caused by subtracting 3 from 2 for 'max_pooling2d_2/MaxPool' (op: 'MaxPool') with input shapes: [?,2,2,256].

LeNet:
    https://medium.com/@mgazar/lenet-5-in-9-lines-of-code-using-keras-ac99294c8086

DECNN:
    https://www.matthewzeiler.com/mattzeiler/deconvolutionalnetworks.pdf

    We have introduced Deconvolutional Networks: a conceptually simple framework for learning sparse, overcomplete feature hierarchies. Applying this framework
    to natural images produces a highly diverse set of filters
    that capture high-order image structure beyond edge primitives. These arise without the need for hyper-parameter
    tuning or additional modules, such as local contrast normalization, max-pooling and rectification [9]. Our approach
    relies on robust optimization techniques to minimize the
    poorly conditioned cost functions that arise in the convolutional setting. Supplemental images, video, and code can
    be found at: http://www.cs.nyu.edu/˜zeiler/
    pubs/cvpr2010/.

conditioned cost functions that arise in the convolutional