# Papers    

Meta - learning

* Learning Transferable Architectures for Scalable Image Recognition
    - https://arxiv.org/abs/1707.07012
* AutoAugment: Learning Augmentation Policies from Data
  - Data augmentation is an effective technique for improving the accuracy of modern image classifiers. However, current data augmentation implementations are manually designed. In this paper, we describe a simple procedure called AutoAugment to automatically search for improved data augmentation policies. In our implementation, we have designed a search space where a policy consists of many sub-policies, one of which is randomly chosen for each image in each mini-batch. A sub-policy consists of two operations, each operation being an image processing function such as translation, rotation, or shearing, and the probabilities and magnitudes with which the functions are applied. We use a search algorithm to find the best policy such that the neural network yields the highest validation accuracy on a target dataset. Our method achieves state-of-the-art accuracy on CIFAR-10, CIFAR-100, SVHN, and ImageNet (without additional data). On ImageNet, we attain a Top-1 accuracy of 83.5% which is 0.4% better than the previous record of 83.1%. On CIFAR-10, we achieve an error rate of 1.5%, which is 0.6% better than the previous state-of-the-art. Augmentation policies we find are transferable between datasets. The policy learned on ImageNet transfers well to achieve significant improvements on other datasets, such as Oxford Flowers, Caltech-101, Oxford-IIT Pets, FGVC Aircraft, and Stanford Cars.
    - @article{DBLP:journals/corr/abs-1805-09501,
        author    = {Ekin Dogus Cubuk and
                    Barret Zoph and
                    Dandelion Man{\'{e}} and
                    Vijay Vasudevan and
                    Quoc V. Le},
        title     = {AutoAugment: Learning Augmentation Policies from Data},
        journal   = {CoRR},
        volume    = {abs/1805.09501},
        year      = {2018},
        url       = {http://arxiv.org/abs/1805.09501},
        archivePrefix = {arXiv},
        eprint    = {1805.09501},
        timestamp = {Mon, 13 Aug 2018 16:48:44 +0200},
        biburl    = {https://dblp.org/rec/bib/journals/corr/abs-1805-09501},
        bibsource = {dblp computer science bibliography, https://dblp.org}
        }
* Searching for Activation Functions
    - https://arxiv.org/abs/1710.05941
    - The choice of activation functions in deep networks has a significant effect on the training dynamics and task performance. Currently, the most successful and widely-used activation function is the Rectified Linear Unit (ReLU). Although various hand-designed alternatives to ReLU have been proposed, none have managed to replace it due to inconsistent gains. In this work, we propose to leverage automatic search techniques to discover new activation functions. Using a combination of exhaustive and reinforcement learning-based search, we discover multiple novel activation functions. We verify the effectiveness of the searches by conducting an empirical evaluation with the best discovered activation function. Our experiments show that the best discovered activation function, f(x)=x⋅sigmoid(βx), which we name Swish, tends to work better than ReLU on deeper models across a number of challenging datasets. For example, simply replacing ReLUs with Swish units improves top-1 classification accuracy on ImageNet by 0.9\% for Mobile NASNet-A and 0.6\% for Inception-ResNet-v2. The simplicity of Swish and its similarity to ReLU make it easy for practitioners to replace ReLUs with Swish units in any neural network.
    - @article{DBLP:journals/corr/abs-1710-05941,
        author    = {Prajit Ramachandran and
                    Barret Zoph and
                    Quoc V. Le},
        title     = {Searching for Activation Functions},
        journal   = {CoRR},
        volume    = {abs/1710.05941},
        year      = {2017},
        url       = {http://arxiv.org/abs/1710.05941},
        archivePrefix = {arXiv},
        eprint    = {1710.05941},
        timestamp = {Mon, 13 Aug 2018 16:48:44 +0200},
        biburl    = {https://dblp.org/rec/bib/journals/corr/abs-1710-05941},
        bibsource = {dblp computer science bibliography, https://dblp.org}
        }
* Deep Residual Learning for Image Recognition
    - https://arxiv.org/abs/1512.03385
    - Deeper neural networks are more difficult to train. We present a residual learning framework to ease the training of networks that are substantially deeper than those used previously. We explicitly reformulate the layers as learning residual functions with reference to the layer inputs, instead of learning unreferenced functions. We provide comprehensive empirical evidence showing that these residual networks are easier to optimize, and can gain accuracy from considerably increased depth. On the ImageNet dataset we evaluate residual nets with a depth of up to 152 layers---8x deeper than VGG nets but still having lower complexity. An ensemble of these residual nets achieves 3.57% error on the ImageNet test set. This result won the 1st place on the ILSVRC 2015 classification task. We also present analysis on CIFAR-10 with 100 and 1000 layers. 
    The depth of representations is of central importance for many visual recognition tasks. Solely due to our extremely deep representations, we obtain a 28% relative improvement on the COCO object detection dataset. Deep residual nets are foundations of our submissions to ILSVRC & COCO 2015 competitions, where we also won the 1st places on the tasks of ImageNet detection, ImageNet localization, COCO detection, and COCO segmentation.
    - @article{DBLP:journals/corr/HeZRS15,
        author    = {Kaiming He and
                    Xiangyu Zhang and
                    Shaoqing Ren and
                    Jian Sun},
        title     = {Deep Residual Learning for Image Recognition},
        journal   = {CoRR},
        volume    = {abs/1512.03385},
        year      = {2015},
        url       = {http://arxiv.org/abs/1512.03385},
        archivePrefix = {arXiv},
        eprint    = {1512.03385},
        timestamp = {Wed, 17 Apr 2019 17:23:45 +0200},
        biburl    = {https://dblp.org/rec/bib/journals/corr/HeZRS15},
        bibsource = {dblp computer science bibliography, https://dblp.org}
        }
* Self-Attention Generative Adversarial Networks
    - https://arxiv.org/pdf/1805.08318.pdf
* efficientNet ?
* bigGan ?
* acgan ? 
* Vanishing gradient




> images important for thesis in gan/img