matplotlib
numpy
torch
torchvision
pillow

flask

# tensorboardX
tensorflow
tensorboard

keras
argparse
scikit-image