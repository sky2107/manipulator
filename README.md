# Project documentation
TODO folder structure displa HOW
1. class Manipulator
2. class Normalize
3. class service

* Transfer learning
    - https://github.com/tomahim/py-image-dataset-generator

> See KanBan with vscode

    press F1
    Open Board

> Conda cmd

    conda activate ann
    cd Documents\Manipulator
    python mani.py or normalize.py

### Data augmentation for complex data

> Data augmentation <br>
> 1,500 item x 8 classes <br>
> Total item 12,000 <br>

1. Balancing dataset
2. Cross validation 80/10/10
    - both 10 % are for validation and testing
3. Data splitting 80/50/30/10
    - always in the bigger datasets
4. Manipulation of all four datasets
5. Get all specs of datasets
    - avg size img
    - min size img
6. normalize data
    - with all thee batches types and two channels
    - 32, 64, 128 batch_size and 1 or 3 channels

### visualisation TBoard

> public folder is the visualisation with tensor board <br>
> to get a feel for the data <br>

    # into the public folder
    # execute this folder with the correct specifications
    python data_visualisation.py
    # than activate tensorboard
    tensorboard --logdir=logs

> classes for the use of data_visualisation.py

1. Folder models are the Networks eg like LeNEt, AlexNet and so on
   - must set flag before argparser
2. transformer_for_data -> class TransformerPYTorch

> Keras
> https://keras.io/visualization/

#

## Screenshots Simple data 1st training plus GAN

> Screenshots data <br>
> folder with original screenshots and randomly picked pictures from the web <br>
> This dataset is simple and for testing or valuation <br>
> The folder with the images of the web must be used

## Gan

> GOAL Screenshots Simple
https://towardsdatascience.com/gan-by-example-using-keras-on-tensorflow-backend-1a6d515a60d0
    epochs = 50000
#

## Service Running

> loaclhost on computerhttp://127.0.0.1:5000/ <br>
> CAREFUL on ubuntu bind to 0.0.0.0

#

## CNN 
  
> NOT enough time
> path model/CNN


1. AlexNet 
2. VGG
3. ResNet
4. SqueezeNet
5. DenseNet
6. Inception v3

> Some models use modules which have different training and evaluation behavior, such as batch normalization. 
> To switch betweenthese modes, use model.train() or model.eval() as appropriate. See train() or eval() for details.
> <br>
> All pre-trained models expect input images normalized in the same way, i.e. mini-batches of 3-channel RGB images of shape (3 
> x H x W), where H and W are expected to be at least 224. The images have to be loaded in to a range of [0, 1] and then 
> normalized using mean = [0.485, 0.456, 0.406] and std = [0.229, 0.224, 0.225]. You can use the following transform to 
> normalize:

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
   
# 

### Kanban 

> https://github.com/mkloubert/vscode-kanban#filter-