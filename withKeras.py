import tensorflow as tf
import numpy as np

from public.model.transformerKeras import LoaderKeras
from public.components.vgg import getVGGNet, train
from public.components.inceptionNet import getInceptionNet
from public.components.resNet import getResNet

from public.components.imageGene import imgLoader, trainWithGen

import os

train_datagen = tf.compat.v1.keras.preprocessing.image.ImageDataGenerator(rescale=1.0 / 255)
train_generator = train_datagen.flow_from_directory(
    "C:\\Users\\FelixNavas\\Documents\\Manipulator\\original\\ten_percent_of_natural_images", target_size=(200, 200), batch_size=32, class_mode="sparse"
)
img_height = 200
img_width = 200
num_classes = 8

base_model = tf.keras.applications.resnet50.ResNet50(weights= None, include_top=False, input_shape= (img_width,img_height,3))
x = base_model.output
x = tf.keras.layers.GlobalAveragePooling2D()(x)
x = tf.keras.layers.Dropout(0.7)(x)
predictions = tf.keras.layers.Dense(num_classes, activation= 'softmax')(x)
model2 = tf.keras.models.Model(inputs = base_model.input, outputs = predictions)


# ? tf.image.per_image_standardization(image)
# min values
inception_input = (150, 150, 3)
resnet_input = (200, 200, 3)
vgg_input = (200, 200, 3)

# model = getVGGNet()
# model = getInceptionNet()
# model = getResNet()
model2.compile(
    optimizer=tf.keras.optimizers.SGD(lr=0.01, momentum=0.9, decay=0.0, nesterov=True),
    loss=tf.keras.losses.sparse_categorical_crossentropy,
    metrics=["accuracy"],
)
model2.fit_generator(
    train_generator,
    steps_per_epoch=4,
    epochs=10
)
model.summary()
dtloader = LoaderKeras('./data/natural_images')
valLoader = LoaderKeras('./validation/natural_images')

# img_train_loader = imgLoader('./data/natural_images')
# img_val_loader = imgLoader("./validation/natural_images")


# trainWithGen(model, img_train_loader)
# exit()
train(model, dtloader, valLoader)

