from components.alexNet_keras import alexNet

if __name__ == "__main__":
    input_shape = (227,227,3)
    n_classe = 8
    opt = 'sgd'
    net = alexNet(input_shape, n_classe, opt)

    net.summary()