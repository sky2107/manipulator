#!/usr/bin/python
# Filename: setup_hyperparameters.py

# For setting flags
import argparse

parser = argparse.ArgumentParser()
#####################################################################################################################################
# path_of_dataset REQUIRED TRAIN/VAL/TEST SETS path to load
parser.add_argument("--path_of_train_data", default='./data/natural_images', type=str, help="path of dataset for training")
parser.add_argument("--path_of_val_data", default='./validation/natural_images', type=str, help="path of dataset for validation")
parser.add_argument("--path_of_test_data", default='', type=str, help="path of dataset for testing final")
#####################################################################################################################################
parser.add_argument("--tboard", type=bool, default=False , help="TBoard off def. FALSE")
parser.add_argument("--json_file", type=bool, default=False, help="Json file off def. FALSE")
parser.add_argument("--saving_model", type=bool, default=True, help="saving model")
parser.add_argument("--loading_model", type=bool, default=False, help="loading model")
parser.add_argument("--loading_path", type=str, default='', help="path of loading model")
#####################################################################################################################################
parser.add_argument("--cnn_name", type=str, default='AlexNet', help="choose which CNN LeNet, AlexNet")
parser.add_argument("--problem", type=str, default='natural_images', help="which dataset or problem")
# Training parameters epochs and batch size
parser.add_argument("--epochs", type=int, default=10, help="number of epochs of training 50 - 10 at the moment")
parser.add_argument("--batch_size", type=int, default=32, help="size of the batches 32, 64 or 128")
parser.add_argument("--number_of_classes", type=int, default=8, help="Classification 2nd dataset 8 classes and 1st 10 digits 0 - 9")
#####################################################################################################################################
# Optimizer Adam or SGD + Nesterov, Momentum
parser.add_argument("--optimizer", type=str, default='Adam', help="Adam or SGD with Nesterov and momentum")
parser.add_argument("--momentum", type=float, default=0.9, help="momentum default 0.9")
parser.add_argument("--nesterov", type=bool, default=True, help="SGD with Nesterov and momentum is so good as Adam")
parser.add_argument("--weight_decay", type=float, default=0, help="Adam or SGD with Nesterov and momentum")
# Loss function
parser.add_argument("--criterion", type=str, default='CrossEntropyLoss', help="Loss function")
# Learning parameters
parser.add_argument("--lr", type=float, default=0.001, help="adam: learning rate")
parser.add_argument("--b1", type=float, default=0.9, help="adam: decay of first order momentum of gradient")
parser.add_argument("--b2", type=float, default=0.999, help="adam: decay of first order momentum of gradient")
# image dimension  plus width and height
parser.add_argument("--input_shape", type=tuple, default=(240, 240, 3), help="input_shape W, H, C")
parser.add_argument("--img_pixel_w", type=int, default=240, help="pixel_w")
parser.add_argument("--img_pixel_h", type=int, default=240, help="pixel_h")
parser.add_argument("--channels", type=int, default=3, help="number of image channels")
# cnn techniques
parser.add_argument("--pooling", type=str, default='max', help="average or max pooling")
parser.add_argument("--filter", type=int, default=3, help="filter for conv operation")
# Dropout conv und Linear difference purpose
parser.add_argument("--dropout", type=bool, default=True, help="dropout")
parser.add_argument("--p_dropout", type=float, default=0.2, help="possibility of droping out")
# Inizilasation Kaiming, Xavier or HE TODO
parser.add_argument("--init_lr_para", type=str, default='Kaiming', help="initialization for lr parameters weights and bias")
#####################################################################################################################################
# layers are not in use at the moment
#####################################################################################################################################
# conv layer plus number of neurons
parser.add_argument("--out_conv1", type=int, default=6, help="output conv 1")
parser.add_argument("--out_conv2", type=int, default=16, help="output conv 2")
parser.add_argument("--out_conv3", type=int, default=384, help="output conv 3")
parser.add_argument("--out_conv4", type=int, default=256, help="output conv 4")
# fc layer no of neurons or connection
parser.add_argument("--out_fc1", type=int, default=120, help="output fc 1")
parser.add_argument("--out_fc2", type=int, default=4096, help="output fc 2")
parser.add_argument("--out_fc3", type=int, default=120, help="output fc 3")
#####################################################################################################################################

version = '0.1'
opt = parser.parse_args()
# End of setup_hyperparameters.py