from tensorboardX import SummaryWriter
# SummaryWriter encapsulates everything
writer = SummaryWriter('runs/exp-1')
# creates writer object. The log will be saved in 'runs/exp-1'
writer2 = SummaryWriter()
# creates writer2 object with auto generated file name,
# the dir will be something like 'runs/Aug20-17-20-33'
writer3 = SummaryWriter(comment='3x learning rate')
# creates writer3 object with auto generated file name, the comment will be appended to the filename.
# The dir will be something like 'runs/Aug20-17-20-33-3xlearning rate'
