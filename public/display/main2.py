# Import Libraries
import torch
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

from logger import Logger

# Specify transforms using torchvision.transforms as transforms
# library
# transformations = transforms.Compose([
#     transforms.Resize((30,30)),
#     # transforms.CenterCrop(224),
#     transforms.ToTensor(),
#     # TODO GET MORE INFORMATION
#     transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
# ])

transformations = transforms.Compose([
    transforms.Resize((30, 30)),
    # transforms.Resize(255),
    # transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
])

path_train_data = '../data/label'
path_val_data = '../data/val_data'

# Load in each dataset and apply transformations using
# the torchvision.datasets as datasets library
train_set = datasets.ImageFolder(path_train_data, transform=transformations)
val_set = datasets.ImageFolder(path_val_data, transform=transformations)
# print(train_set)
# Put into a Dataloader using torch library
train_loader = torch.utils.data.DataLoader(
    train_set, batch_size=64, shuffle=True)
val_loader = torch.utils.data.DataLoader(val_set, batch_size=1, shuffle=True)

# ------------------------------------------------------------------------------------
# # TODO
# # Get pretrained model using torchvision.models as models library
# model = models.densenet161(pretrained=True)
# # Turn off training for their parameters
# for param in model.parameters():
#     param.requires_grad = False

# TODO
classifier_input = 3

num_labels = 10
# TODO
# size mismatch, m1: [32 x 2208], m2: [3 x 1024]
# Create new classifier for model using torch.nn as nn library
# classifier = nn.Sequential(nn.Linear(classifier_input, 1024),
#                            nn.ReLU(),
#                            nn.Linear(2208, 512),
#                            nn.ReLU(),
#                            nn.Linear(512, num_labels),
#                            nn.LogSoftmax(dim=1))
# # Replace default classifier with new classifier
# model.classifier = classifier

# ------------------------------------------------------------------------------------


class Net(nn.Module):
    def __init__(self):
        self.num_features = 1
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        # size mismatch, m1: [64 x 56180], m2: [320 x 50]
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, self.flatten(x))
        x = F.relu(self.fc1(x))
        # TODO training
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)

    def flatten(self, x):
        num_features = 1
        for s in x.size()[1:]:
            num_features *= s
        self.num_features = num_features
        return num_features


model = Net()

logger = Logger('./logs')

# Find the device available to use using torch library
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# Move model to the device specified above
model.to(device)

# Set the error function using torch.nn as nn library
criterion = nn.NLLLoss()
# Set the optimizer function using torch.optim as optim library
# optimizer = optim.Adam(model.classifier.parameters())
optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
loss = 0
epochs = 10

for epoch in range(epochs):
    train_loss = 0
    val_loss = 0
    accuracy = 0

    # Training the model
    model.train()
    counter = 0
    for inputs, labels in train_loader:
        # Move to device
        inputs, labels = inputs.to(device), labels.to(device)
        # Clear optimizers

        optimizer.zero_grad()
        # print(inputs.shape)
        # Forward pass
        output = model.forward(inputs)
        # Loss
        loss = criterion(output, labels)
        # Calculate gradients (backpropogation)
        loss.backward()
        # Adjust parameters based on gradients
        optimizer.step()
        # Add the loss to the training set's rnning loss
        train_loss += loss.item()*inputs.size(0)

        # Print the progress of our training
        counter += 1
        print(counter, "/", len(train_loader))

        if counter == None:# (len(train_loader) - 1)
            # ================================================================== #
            #                        Tensorboard Logging                         #
            # ================================================================== #

            # 1. Log scalar values (scalar summary) # accuracy.item()
            # info = {'loss': loss.item(), 'accuracy': accuracy.item()}

            # for tag, value in info.items():
            #     logger.scalar_summary(tag, value, counter+1)

            # # 2. Log values and gradients of the parameters (histogram summary)
            # for tag, value in model.named_parameters():
            #     tag = tag.replace('.', '/')
            #     logger.histo_summary(tag, value.data.cpu().numpy(), counter+1)
            #     logger.histo_summary(
            #         tag+'/grad', value.grad.data.cpu().numpy(), counter+1)

            # # 3. Log training images (image summary)
            # info = {'images': images.view(-1, 28, 28)[:10].cpu().numpy()}

            # for tag, images in info.items():
            #     logger.image_summary(tag, images, counter+1)
            pass

    # Evaluating the model
    model.eval()
    counter = 0
    # Tell torch not to calculate gradients
    with torch.no_grad():
        for inputs, labels in val_loader:
            # Move to device
            inputs, labels = inputs.to(device), labels.to(device)
            # Forward pass
            output = model.forward(inputs)
            # Calculate Loss
            valloss = criterion(output, labels)
            # Add loss to the validation set's running loss
            val_loss += valloss.item()*inputs.size(0)

            # Since our model outputs a LogSoftmax, find the real
            # percentages by reversing the log function
            output = torch.exp(output)
            # Get the top class of the output
            top_p, top_class = output.topk(1, dim=1)
            # See how many of the classes were correct?
            equals = top_class == labels.view(*top_class.shape)
            # Calculate the mean (get the accuracy for this batch)
            # and add it to the running accuracy for this epoch
            accuracy += torch.mean(equals.type(torch.FloatTensor)).item()

            # Print the progress of our evaluation
            counter += 1
            print(counter, "/", len(val_loader))
            # ================================================================== #
            #                        Tensorboard Logging                         #
            # ================================================================== #

            # 1. Log scalar values (scalar summary) # accuracy.item()
            info = {'loss': valloss.item(), 'accuracy': torch.mean(equals.type(torch.FloatTensor)).item()}

            for tag, value in info.items():
                logger.scalar_summary(tag, value, counter+1)

            # 2. Log values and gradients of the parameters (histogram summary)
            for tag, value in model.named_parameters():
                tag = tag.replace('.', '/')
                logger.histo_summary(tag, value.data.cpu().numpy(), counter+1)
                logger.histo_summary(
                    tag+'/grad', value.grad.data.cpu().numpy(), counter+1)

            # 3. Log training images (image summary)
            print(info)
            print(type(info))
            for tag, images in info.items():
                logger.image_summary(tag, inputs, counter+1)

    # Get the average loss for the entire epoch
    train_loss = train_loss/len(train_loader.dataset)
    valid_loss = val_loss/len(val_loader.dataset)
    # Print out the information
    print('Accuracy: ', accuracy/len(val_loader))
    print('Epoch: {} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f}'.format(
        epoch, train_loss, valid_loss))
    loss = train_loss
