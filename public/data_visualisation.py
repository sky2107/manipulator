# tensorboard with pytorch
# Import Libraries
# from torch.utils.tensorboard import SummaryWriter
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torch.optim as optim
from PIL import Image
import numpy as np
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
import timeit

import json
import time
import os
import re
import shutil

from tensorboardX import SummaryWriter

# import networks LeNet, AlexNet
from model.leNet import LeNet
from model.alexNet import AlexNet
from model.transformerPYTorch import TransformerPYTorch


print(torch.__version__)
print(torchvision.__version__)

torch.set_printoptions(linewidth=120)
torch.set_grad_enabled(True)


# Parameters of dataset set up with argparser
pixel_w = 30
pixel_h = 30
channels = 3
# pooling = 'average'
pooling = 'max'
number_of_classes = 10
dropout = True
p_dropout = 0.2
input_shape = (pixel_w, pixel_h, channels)
# Diffrent data types
# path_of_data = './../data_augmentation/natural_images'
# path_of_data = 'C:\\Users\\FelixNavas\\Documents\\manipulator\\original\\natural_images/dog/dog_0001.jpg'
# path_of_data = 'C:\\Users\\FelixNavas\\Documents\\Manipulator\\data\\screenshots\\0\\0_0_ImageEnhance_Brightness_12.jpg'

path_of_img = 'C:\\Users\\FelixNavas\\Documents\\Manipulator\\data\\screenshots\\0\\010.jpg'
path_of_data = 'C:\\Users\\FelixNavas\\Documents\\Manipulator\\data\\screenshots\\'
batch_size = 32

# tboard set up
logging_path_for_tboard = 'C:\\Users\\FelixNavas\\Documents\\Manipulator\\public\\logs'

TBoard_is_on = False
if TBoard_is_on:
    writer = SummaryWriter(logging_path_for_tboard)


# Find the device available to use using torch library
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

tformer = TransformerPYTorch()

# Process Image default path already init. with one image
image = tformer.process_image(path_of_img, pixel_w=pixel_w, pixel_h=pixel_h)

le_net = LeNet(number_of_classes=number_of_classes, input_shape=input_shape,
               pooling=pooling, dropout=dropout, p_dropout=p_dropout)

# Preferences of the network
# print(le_net)

# le_net.eval()

# output = le_net(image)

# alex_net = AlexNet(number_of_classes=number_of_classes, input_shape=input_shape,
#                    pooling=pooling, dropout=dropout, p_dropout=p_dropout)
# print(alex_net)

# alex_net.eval()
# output = alex_net(image)

print('Input shape')
print(image.shape)


train_loader = tformer.dataTransformer(
    pixel_w, pixel_h, channels, path_of_data, batch_size)

# # batch size doesn’t matter the generator resamples
# iterator = tformer.getIterator(train_loader)

# # Iterator TYPE
# print(type(iterator))
# images, labels = tformer.nextIterationForDataset(iterator)
# count = 0
# # infinite loop
# while True:
#     try:
#         count += 1
#         # get the next item
#         images, labels = tformer.nextIterationForDataset(iterator)
#         #############################################################################################
#         # TBoard
#         #############################################################################################
#         writer.add_graph(le_net, images)
#         # tformer.showGrid(batch_size, images)
#         grid_img = tformer.showGrid(batch_size, images)
#         # writer.add_graph(le_net, images)
#         writer.add_image('images_' + str(count), grid_img, count)
#         print(count)
#         # do something with element
#     except StopIteration:
#         # if StopIteration is raised, break from loop
#         break


# exit()

count = 0
le_net.train()
for batch_idx, (images, labels) in enumerate(train_loader):
    count += 1
    print(count)


# writer.close()