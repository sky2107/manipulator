import tensorflow as tf

def alexNet(in_shape=(227,227,3), n_classes=1000, opt='sgd'):
    in_layer = tf.keras.layers.Input(in_shape)
    conv1 = tf.keras.layers.Conv2D(96, 11, strides=4, activation='relu')(in_layer)
    pool1 = tf.keras.layers.MaxPool2D(3, 2)(conv1)
    conv2 = tf.keras.layers.Conv2D(256, 5, strides=1, padding='same', activation='relu')(pool1)
    pool2 = tf.keras.layers.MaxPool2D(3, 2)(conv2)
    conv3 = tf.keras.layers.Conv2D(384, 3, strides=1, padding='same', activation='relu')(pool2)
    conv4 = tf.keras.layers.Conv2D(256, 3, strides=1, padding='same', activation='relu')(conv3)
    pool3 = tf.keras.layers.MaxPool2D(3, 2)(conv4)
    flattened = tf.keras.layers.Flatten()(pool3)
    dense1 = tf.keras.layers.Dense(4096, activation='relu')(flattened)
    drop1 = tf.keras.layers.Dropout(0.5)(dense1)
    dense2 = tf.keras.layers.Dense(4096, activation='relu')(drop1)
    drop2 = tf.keras.layers.Dropout(0.5)(dense2)
    preds = tf.keras.layers.Dense(n_classes, activation='softmax')(drop2)

    model = tf.keras.Model(in_layer, preds)
    model.compile(loss="categorical_crossentropy", optimizer=opt,
	              metrics=["accuracy"])
    return model