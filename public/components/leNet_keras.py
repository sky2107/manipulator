import tensorflow
from time import time
import tensorflow as tf

initialization_for_weights = ['he_uniform', 'he_normal']

optim = ['adam']
def lenet(in_shape=(32, 32, 1), n_classes=10, opt='adam', method_of_init='bogiot'):
    if 'bogiot' == method_of_init:
        method_for_w_to_init = 'he_uniform'
    elif 'hinton' == method_of_init:
        method_for_w_to_init = 'he_normal'

    model = tensorflow.keras.Sequential()

    model.add(tf.keras.layers.Conv2D(filters=6, kernel_size=(3, 3), kernel_initializer=method_for_w_to_init,
                                     activation='relu', input_shape=(32, 32, 1)))
    model.add(tf.keras.layers.AveragePooling2D())
    model.add(tf.keras.layers.Conv2D(
        filters=16, kernel_size=(3, 3), kernel_initializer=method_for_w_to_init, activation='relu'))
    model.add(tf.keras.layers.AveragePooling2D())
    model.add(tf.keras.layers.Flatten())
    model.add(
        tf.keras.layers.Dense(units=120, kernel_initializer=method_for_w_to_init, activation='relu'))
    model.add(tf.keras.layers.Dense(
        units=84, kernel_initializer=method_for_w_to_init, activation='relu'))
    model.add(tf.keras.layers.Dense(units=8, activation='softmax'))

    # optim
    model.compile(loss="categorical_crossentropy", optimizer=opt,
	              metrics=["accuracy"])

    return model

def lenet_RGB(in_shape=(32, 32, 3), n_classes=10, opt='adam', method_of_init='bogiot'):
    if 'bogiot' == method_of_init:
        method_for_w_to_init = 'he_uniform'
    elif 'hinton' == method_of_init:
        method_for_w_to_init = 'he_normal'

    model = tensorflow.keras.Sequential()

    model.add(tf.keras.layers.Conv2D(filters=6, kernel_size=(3, 3), kernel_initializer=method_for_w_to_init,
                                     activation='relu', input_shape=(32, 32, 3)))
    model.add(tf.keras.layers.AveragePooling2D())
    model.add(tf.keras.layers.Conv2D(
        filters=16, kernel_size=(3, 3), kernel_initializer=method_for_w_to_init, activation='relu'))
    model.add(tf.keras.layers.AveragePooling2D())
    model.add(tf.keras.layers.Flatten())
    model.add(
        tf.keras.layers.Dense(units=1200, kernel_initializer=method_for_w_to_init, activation='relu'))
    model.add(tf.keras.layers.Dense(
        units=200, kernel_initializer=method_for_w_to_init, activation='relu'))
    model.add(tf.keras.layers.Dense(units=8, activation='softmax'))

    # optim
    model.compile(loss="categorical_crossentropy", optimizer=opt,
	              metrics=["accuracy"])

    return model