import tensorflow as tf
import os

# ? https://github.com/raghakot/keras-resnet

def getResNet(
    net: str = "ResNet50",
    classes: int = 8,
    pixel_w: int = 200,
    pixel_h: int = 200,
    channel: int = 3, 
    freezing: bool = False
):
    """
        resnet
    """
    input_shape = tuple([pixel_w, pixel_h, channel])
    if net == "ResNet50":
        resnet = tf.keras.applications.resnet50.ResNet50(
            include_top=False,
            weights="imagenet",
            input_shape=input_shape,
            classes=classes,
        )
    elif net == "ResNet101":
        resnet = tf.keras.applications.resnet.ResNet101(
            include_top=False,
            weights="imagenet",
            input_shape=input_shape,
            classes=classes,
        )

    resnet.summary()

    model = tf.keras.Sequential()
    # freezing convs
    for layer in resnet.layers:
        if freezing:
            layer.trainable = False
        model.add(layer)
    model.add(tf.keras.layers.GlobalAveragePooling2D())
    model.add(tf.keras.layers.Dropout(0.7))
    model.add(tf.keras.layers.Dense(classes, activation=tf.compat.v1.math.log_softmax))

    return model


def train(model, dtloader, optim: str = "SGD", epochs: int = 30, saving: bool = False):
    """
        imagenet
    """
    if optim == "SGD":
        model.compile(
            optimizer=tf.keras.optimizers.SGD(
                lr=0.01, momentum=0.9, decay=0.0, nesterov=True
            ),
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )
    elif optim == "Adam":
        model.compile(
            optimizer=tf.keras.optimizers.Adam(
                lr=0.001,
                beta_1=0.9,
                beta_2=0.999,
                epsilon=None,
                decay=0.0,
                amsgrad=False,
            ),
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )

    model.summary()
    
    steps_per_epoch = 3
    
    if saving:
        checkpoint_path = "./cp-{epoch:04d}.ckpt.ckpt"
        checkpoint_dir = os.path.dirname(checkpoint_path)

        # Create checkpoint callback
        cp_callback = tf.keras.callbacks.ModelCheckpoint(
            checkpoint_path, save_weights_only=True, verbose=1, period=5
        )
        model.fit(dtloader.ds, epochs=epochs, steps_per_epoch=dtloader.steps_per_epoch, callbacks = [cp_callback])
    else:
        model.fit(dtloader.ds, epochs=epochs, steps_per_epoch=dtloader.steps_per_epoch)


