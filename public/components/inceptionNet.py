import tensorflow as tf
import os


def getInceptionNet(
    net: str = "InceptionV3",
    classes: int = 8,
    pixel_w: int = 200,
    pixel_h: int = 200,
    channel: int = 3,
    freezing: bool = False
):
    """
        imagenet
    """
    input_shape = tuple([pixel_w, pixel_h, channel])
    if net == "InceptionV3":
        resnet = tf.keras.applications.inception_v3.InceptionV3(
            include_top=False,
            weights="imagenet",
            input_shape=(200,200,3),
            classes=classes,
        )
    elif net == "InceptionResNetV2":
        resnet = tf.keras.applications.inception_resnet_v2.InceptionResNetV2(
            include_top=False,
            weights="imagenet",
            input_shape=(200,200,3),
            classes=classes,
        )

    # resnet.summary()

    model = tf.keras.Sequential()
    _input = tf.keras.layers.Input(resnet)
    # freezing convs
    # for layer in resnet.layers:
    #     if freezing:
    #         layer.trainable = False
    #     model.add(layer)
    model.add(_input)
    model.add(tf.keras.layers.Flatten(name="flatten"))
    model.add(tf.keras.layers.Dense(512, activation="relu"))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(classes, activation=tf.compat.v1.math.log_softmax))
    
    model.summary()

    return model


def train(model, dtloader, optim: str = "SGD", epochs: int = 30, saving: bool = False):
    """
        imagenet
    """
    if optim == "SGD":
        model.compile(
            optimizer=tf.keras.optimizers.SGD(
                lr=0.01, momentum=0.9, decay=0.0, nesterov=True
            ),
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )
    elif optim == "Adam":
        model.compile(
            optimizer=tf.keras.optimizers.Adam(
                lr=0.001,
                beta_1=0.9,
                beta_2=0.999,
                epsilon=None,
                decay=0.0,
                amsgrad=False,
            ),
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )

    model.summary()
    
    steps_per_epoch = 3
    
    if saving:
        checkpoint_path = "./cp-{epoch:04d}.ckpt.ckpt"
        checkpoint_dir = os.path.dirname(checkpoint_path)

        # Create checkpoint callback
        cp_callback = tf.keras.callbacks.ModelCheckpoint(
            checkpoint_path, save_weights_only=True, verbose=1, period=5
        )
        model.fit(dtloader.ds, epochs=epochs, steps_per_epoch=dtloader.steps_per_epoch, callbacks = [cp_callback])
    else:
        model.fit(dtloader.ds, epochs=epochs, steps_per_epoch=dtloader.steps_per_epoch)


