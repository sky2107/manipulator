import tensorflow as tf
import os
import datetime

def getVGGNet(
    net: str = "vgg19",
    classes: int = 8,
    pixel_w: int = 240,
    pixel_h: int = 240,
    channel: int = 3,
    freezing: bool = False
):
    """
        imagenet
    """
    input_shape = tuple([pixel_w, pixel_h, channel])
    if net == "vgg19":
        vgg = tf.keras.applications.vgg19.VGG19(
            include_top=False,
            weights="imagenet",
            input_shape=input_shape,
            classes=classes,
        )
    elif net == "vgg16":
        vgg = tf.keras.applications.vgg16.VGG16(
            include_top=False,
            weights="imagenet",
            input_shape=input_shape,
            classes=classes,
        )

    vgg.summary()

    model = tf.keras.Sequential()
    # freezing convs
    for layer in vgg.layers:
        if freezing:
            layer.trainable = False
        model.add(layer)

    model.add(tf.keras.layers.Flatten(name="flatten"))
    model.add(tf.keras.layers.Dense(512, activation="relu"))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(classes, activation=tf.compat.v1.math.log_softmax))

    return model


def train(model, dtloader, valLoader, optim: str = "SGD", epochs: int = 10, saving: bool = False):
    """
        imagenet
    """
    if optim == "SGD":
        model.compile(
            optimizer=tf.keras.optimizers.SGD(
                lr=0.01, momentum=0.9, decay=0.0, nesterov=True
            ),
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )
    elif optim == "Adam":
        model.compile(
            optimizer=tf.keras.optimizers.Adam(
                lr=0.001,
                beta_1=0.9,
                beta_2=0.999,
                epsilon=None,
                decay=0.0,
                amsgrad=False,
            ),
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )

    model.summary()
    
    steps_per_epoch = 3
    
    if saving:
        checkpoint_path = "./cp-{epoch:04d}.ckpt.ckpt"
        checkpoint_dir = os.path.dirname(checkpoint_path)

        # Create checkpoint callback
        cp_callback = tf.keras.callbacks.ModelCheckpoint(
            checkpoint_path, save_weights_only=True, verbose=1, period=5
        )
        model.fit(dtloader.ds, epochs=epochs, steps_per_epoch=dtloader.steps_per_epoch, callbacks = [cp_callback])
    else:
        # log_dir="C:\\Users\\FelixNavas\\Documents\\Manipulator/public/logs/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        # tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)
        model.fit(dtloader.ds, epochs=epochs, steps_per_epoch=30, validation_data=valLoader.ds, validation_steps=1) #, callbacks = [tensorboard_callback])

