import tensorflow as tf
import pathlib
import os

import random


def imgLoader(
    path_of_data: str = "C:\\Users\\FelixNavas\\Documents\\Manipulator\\data\\ten_percent_of_natural_images"
):
    img_data_generator = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1./255)

    data_root = pathlib.Path(path_of_data)
    all_image_paths = list(data_root.glob("*/*"))
    all_image_paths = [str(path) for path in all_image_paths]

    random.shuffle(all_image_paths)

    image_count = len(all_image_paths)

    label_names = sorted(item.name for item in data_root.glob("*/") if item.is_dir())
    directory = path_of_data
    dt = img_data_generator.flow_from_directory(
        directory,
        target_size=(200, 200),
        color_mode="rgb",
        class_mode="categorical",
        batch_size=32,
        shuffle=True
    )

    return dt

def trainWithGen(model, img_dt, optim: str = "SGD", epochs: int = 1, saving: bool = False):
    """
        img gen
        https://medium.com/@vijayabhaskar96/tutorial-image-classification-with-keras-flow-from-directory-and-generators-95f75ebe5720
    """
    if optim == "SGD":
        model.compile(
            optimizer=tf.keras.optimizers.SGD(
                lr=0.01, momentum=0.9, decay=0.0, nesterov=True
            ),
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )
    elif optim == "Adam":
        model.compile(
            optimizer=tf.keras.optimizers.Adam(
                lr=0.001,
                beta_1=0.9,
                beta_2=0.999,
                epsilon=None,
                decay=0.0,
                amsgrad=False,
            ),
            loss=tf.keras.losses.sparse_categorical_crossentropy,
            metrics=["accuracy"],
        )

    
    if saving:
        pass
    else:
        # log_dir="C:\\Users\\FelixNavas\\Documents\\Manipulator/public/logs/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        # tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)
        print(type(img_dt))
        # model.fit_generator(img_dt, steps_per_epoch=6) # , validation_data=val_it, validation_steps=8) 
        
# featurewise_center=False,
#         samplewise_center=False,
#         featurewise_std_normalization=False,
#         samplewise_std_normalization=False,
#         zca_whitening=False,
#         zca_epsilon=1e-06,
#         rotation_range=0,
#         width_shift_range=0.0,
#         height_shift_range=0.0,
#         brightness_range=None,
#         shear_range=0.0,
#         zoom_range=0.0,
#         channel_shift_range=0.0,
#         fill_mode="nearest",
#         cval=0.0,
#         horizontal_flip=False,
#         vertical_flip=False,
#         rescale=None,
#         preprocessing_function=None,
#         data_format=None,
#         validation_split=0.0,
#         dtype=None,