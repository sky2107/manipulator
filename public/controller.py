# Import Libraries
import json
import os
from functools import reduce
from sys import platform
from time import time

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.optim as optim

from config.setup_hyperparameters import opt
from model.alexNet import AlexNet
from model.leNet import LeNet
from model.transformerPYTorch import TransformerPYTorch

# For both systems server and computer
if platform == "linux" or platform == "linux2":
    # linux
    from tensorboardX import SummaryWriter
    # from torch.utils.tensorboard import SummaryWriter
elif platform == "darwin":
    # OS X
    pass
elif platform == "win32":
    # Windows...
    from tensorboardX import SummaryWriter


class Controller:
    '''
        Controller
        gets parameters of all Hyberparameters
    '''

    def __init__(self, path_for_tb_data: str = './public/runs/', loading_another_network: bool = False):
        # TIMER
        self.time_right_before_init = time()
        # important for saving the elements
        self.new_path_to_create_folder_structure_to_save_results = self.creatingFolderPathToSaveReults()
        # print(opt.tboard)
        # print(opt)
        # TBoard
        if True:
        # if opt.tboard:
            # for comparisons
            self.train_loss = []
            self.val_loss = []
            ###################################################################
            self.name_of_the_training_folder = self.creatingTBoardFoldersForTheEvents(
                path_for_tb_data)
            self.writer = SummaryWriter(self.name_of_the_training_folder)

        ###################################################################
        # CNN
        self.model = self.getNetwork()
        ###################################################################
        if loading_another_network:
            self.loadingModel(opt.loading_path)
        else:
            # CPU or GPU
            self.model = self.model.to(self.model.device)
            # initialization weight and bias to 0
            self.model.apply(self.model.init_kaimingHE)
            ###################################################################
            # optimizer Adem or SGD
            self.optimizer = self.getOptimizer()
            # criterion CrossEntropyLoss, NLLLoss
            self.criterion = self.getCriterion()
        ###################################################################
        # Data LOADING AREA plus actions
        self.loader = TransformerPYTorch()
        if opt.path_of_train_data != '':
            # ! andrew ng
            # self.validation_loader = self.getDataloader(
            #     opt.path_of_val_data, 1)
            self.train_loader = self.getDataloader(
                opt.path_of_train_data, opt.batch_size)
            # Learning
            self.training()
        # batch size is irrelevant for TEST AND VALIDATION
        if opt.path_of_val_data != '':
            self.validation_loader = self.getDataloader(
                opt.path_of_val_data, 1)
            # valuating
            self.validation()
        if opt.path_of_test_data != '':
            self.test_loader = self.getDataloader(
                opt.path_of_test_data, 1)
            # testing
            self.finalTest()
        ###################################################################

    def getDataloader(self, path_of_data: str, batch_size: str):
        '''
            return data
        '''
        pixel_w = opt.input_shape[0]
        pixel_h = opt.input_shape[1]
        channels = opt.input_shape[2]

        data_loader = self.loader.dataTransformer(
            pixel_w, pixel_h, channels, path_of_data, batch_size)

        return data_loader

    def getNetwork(self):
        print('Creating Network')
        print('------------------------')
        model = None
        list_of_cnns = ['AlexNet', 'LeNet']
        if opt.cnn_name == 'AlexNet':
            model = AlexNet(number_of_classes=opt.number_of_classes, input_shape=opt.input_shape,
                            pooling=opt.pooling, dropout=opt.dropout, p_dropout=opt.p_dropout)
        elif opt.cnn_name == 'LeNet':
            model = LeNet(number_of_classes=opt.number_of_classes, input_shape=opt.input_shape,
                          pooling=opt.pooling, dropout=opt.dropout, p_dropout=opt.p_dropout)
        else:
            raise Exception('Wrong type choose from: ', list_of_cnns)
        print(model)
        # print(type(model))
        print('------------------------')
        print()
        return model

    def getOptimizer(self):
        '''
            optimizer Adem or SGD
            https://medium.com/@Biboswan98/optim-adam-vs-optim-sgd-lets-dive-in-8dbf1890fbdc
        '''
        print('Selecting Optimizer')
        print('------------------------')
        optimizer = None
        list_of_optims = ['Adam', 'SGD']
        if opt.optimizer == 'Adam':
            optimizer = optim.Adam(self.model.parameters(), lr=opt.lr, betas=(
                opt.b1, opt.b2), eps=1e-08, weight_decay=opt.weight_decay, amsgrad=False)
        elif opt.optimizer == 'SGD':
            # In fact it is said that SGD+Nesterov can be as good as Adam’s technique.
            optimizer = optim.SGD(self.model.parameters(), lr=opt.lr, momentum=opt.momentum,
                                  dampening=0, weight_decay=opt.weight_decay, nesterov=opt.nesterov)
        else:
            raise Exception('Wrong optimizer choose from: ', list_of_optims)
        print(optimizer)
        # print(type(optimizer))
        print('------------------------')
        print()
        return optimizer

    def getCriterion(self):
        '''
            Set the optimizer function using torch.optim as optim library
            https://machinelearningmastery.com/loss-and-loss-functions-for-training-deep-learning-neural-networks/
            official site of pytorch
            https://pytorch.org/docs/stable/_modules/torch/nn/modules/loss.html
        '''
        print('Selecting Criterion')
        print('------------------------')
        criterion = None
        list_of_criterions = ['NLLLoss', 'CrossEntropyLoss']
        if opt.criterion == 'NLLLoss':
            criterion = torch.nn.NLLLoss()
        elif opt.criterion == 'CrossEntropyLoss':
            criterion = torch.nn.CrossEntropyLoss()
        else:
            raise Exception('Wrong criterion choose from: ',
                            list_of_criterions)
        print(criterion)
        # print(type(criterion))
        print('------------------------')
        print()
        return criterion

    def training(self):
        '''
            Training the model
        '''
        start_training = time()
        print('Training the model')
        print('------------------------')
        print()
        stats_training = []
        test_counter = [i*len(self.train_loader.dataset)
                        for i in range(opt.epochs + 1)]
        length_of_dataset_incl_batches = len(self.train_loader)
        ten_turn_only = len(self.train_loader) // 10
        turns_till_ten = 1
        first_time = True
        # self.train_loss = []
        # print('test_counter')
        # print(test_counter)
        for epoch in range(opt.epochs):
            train_loss = 0.0
            running_loss = 0.0
            train_accuracy = 0.0
            adding_all_train_accuracy = 0.0
            number_of_correct_elements = 0
            loss_item = .0
            print("Epoch: ".format(epoch + 1 / opt.epochs))
            print('------------------------')
            # Training the model
            self.model.train()

            counter = 1
            for images, labels in self.train_loader:
                # Move to device
                images, labels = images.to(
                    self.model.device), labels.to(self.model.device)
                ###################################################################
                # Clear optimizers
                self.optimizer.zero_grad()
                # Forward pass
                output = self.model.forward(images)
                # Loss
                loss = self.criterion(output, labels)
                # Calculate gradients (backpropogation)
                loss.backward()
                # Adjust parameters based on gradients
                self.optimizer.step()
                # Add the loss to the training set's running loss
                train_loss += loss.item()*images.size(0)
                running_loss += loss.item()
                loss_item = loss.item()

                # Reverse the log function in our output
                output = torch.exp(output)
                # Get the top predicted class, and the output percentage for
                # that class
                probs, classes = output.topk(1, dim=1)
                equals = classes == labels.view(*classes.shape)

                # Calculate the mean (get the accuracy for this batch)
                # and add it to the running accuracy for this epoch
                train_accuracy += torch.mean(
                    equals.type(torch.FloatTensor)).item()
                number_of_correct_elements += torch.sum(
                    equals.type(torch.FloatTensor)).item()

                actual_accuracy_for_this_batch = torch.mean(
                    equals.type(torch.FloatTensor)).item()
                adding_all_train_accuracy += actual_accuracy_for_this_batch

                ###################################################################
                # TBOARD Training
                ###################################################################
                # if opt.tboard:
                if True:
                    if counter % ten_turn_only == 0 and epoch + 1 == opt.epochs:
                        # self.addGraph(images)
                        grid_img = self.loader.showGrid(opt.batch_size, images)
                        self.writer.add_image(
                            'images_' + opt.problem + str(turns_till_ten), grid_img, turns_till_ten)
                        for name, param in self.model.named_parameters():
                            self.writer.add_histogram(
                                name, param, turns_till_ten)
                        # turns_till_ten += 1
                        ###################################################################
                        # self.writer.add_scalar(
                        #     'Training accurancy', actual_accuracy_for_this_batch, turns_till_ten)  # counter)
                        # self.writer.add_scalar(
                        #     'Training just loss.item()', loss.item(), turns_till_ten)  # counter)
                        # self.writer.add_scalar(
                        #     'Training loss for all elments in the batch size', loss.item()*images.size(0), turns_till_ten)  # counter)
                        # self.writer.add_scalar(
                        #     'Training Running loss', running_loss, turns_till_ten)  # counter)
                        # self.writer.add_scalar(
                        #     'Training Actual loss avg', running_loss / counter, turns_till_ten)  # counter)
                        ###################################################################
                        turns_till_ten += 1
                        # ARRAY train loss
                        # self.train_loss.append(loss.item()*images.size(0))
                ###################################################################
                # Print the progress of our training last position to check
                # End of the training set
                if counter == len(self.train_loader):
                    accuracy_for_whole_epoch = train_accuracy / counter
                    # print('Loops during epochs')
                    # print(counter, "/", len(self.train_loader))
                    # https://thepythonguru.com/python-string-formatting/
                    print('epoch {0:d} \t total loss {1:.6f} \t running loss {2:.6f} \t avg loss {3:.6f} \t last loss {4:.6f}'.format(
                        epoch + 1, train_loss, running_loss, running_loss / counter, loss.item()))
                    print('correct guesses {0:d} from {1:d}'.format(
                        int(number_of_correct_elements), len(self.train_loader.dataset)))
                    print('Training accuracy for the epoch was {0:.2f}'.format(
                        accuracy_for_whole_epoch))

                    # if opt.saving_model and accuracy_for_whole_epoch > .8 and first_time:
                    #     self.saveNetwork(epoch + 1, loss)
                    #     first_time = False
                    if epoch + 1 == opt.epochs:
                        # saving last model
                        # ! flag
                        # todo
                        # self.saveNetwork(epoch + 1, loss)
                        print('Time to train model was {0:.2f} seconds'.format(
                            time() - start_training))
                        print()

                counter += 1
            ###################################################################
            # TBOARD Training
            ###################################################################
            # if opt.tboard:
            if True:
                for name, param in self.model.named_parameters():
                    self.writer.add_histogram(
                        name, param, epoch + 1)
                # turns_till_ten += 1
                ###################################################################
                self.writer.add_scalar(
                    'Training accurancy', actual_accuracy_for_this_batch, epoch + 1)  # counter)
                self.writer.add_scalar(
                    'Training just loss.item()', loss_item, epoch + 1)  # counter)
                self.writer.add_scalar(
                    'Training loss for all elments in the batch size', loss_item * opt.batch_size, epoch + 1)  # counter)
                self.writer.add_scalar(
                    'Training Running loss', running_loss, epoch + 1)  # counter)
                self.writer.add_scalar(
                    'Training Actual loss avg', running_loss / len(self.train_loader.dataset), epoch + 1)  # counter)
                ###################################################################
                turns_till_ten += 1
                # ARRAY train loss
                self.train_loss.append(loss_item * opt.batch_size)
            
    def validation(self):
        '''
            Validation the model
        '''
        start_validation = time()
        print('Validating the model')
        print('------------------------')
        stats_validation = []
        # self.val_loss = []
        self.model.eval()
        # val paras
        val_loss = 0.0
        val_accuracy = 0.0
        val_running_loss = 0.0
        number_of_correct_elements = 0
        counter = 1
        ten_turn_only = len(self.validation_loader) // 10
        turn_bt_for_vis = 0
        turns_till_ten = 1
        # Tell torch not to calculate gradients
        with torch.no_grad():
            for images, labels in self.validation_loader:
                # Move to device
                images, labels = images.to(
                    self.model.device), labels.to(self.model.device)
                # Forward pass
                output = self.model.forward(images)
                # Calculate Loss
                loss = self.criterion(output, labels)
                # Add loss to the validation set's running loss
                val_loss += loss.item()*images.size(0)
                val_running_loss += loss.item()

                # Since our model outputs a LogSoftmax, find the real
                # percentages by reversing the log function
                output = torch.exp(output)
                # Get the top class of the output
                top_p, top_class = output.topk(1, dim=1)
                # See how many of the classes were correct?
                equals = top_class == labels.view(*top_class.shape)
                # Calculate the mean (get the accuracy for this batch)
                # and add it to the running accuracy for this epoch
                # val_accuracy += torch.mean(equals.type(torch.FloatTensor)).item()
                number_of_correct_elements += torch.sum(
                    equals.type(torch.FloatTensor)).item()
                val_accuracy += torch.mean(
                    equals.type(torch.FloatTensor)).item()
                ###################################################################
                # TBOARD Validation
                ###################################################################
                # if opt.tboard:
                if True:
                    ###################################################################
                    # after 10
                    if counter % ten_turn_only == 0:
                        # if (len(self.validation_loader) // 10) % counter == 0:
                        # print(turns_till_ten)
                        self.writer.add_scalar('Validation accurancy', torch.mean(
                            equals.type(torch.FloatTensor)).item(), turns_till_ten)
                        self.writer.add_scalar(
                            'Validation running loss', val_running_loss, turns_till_ten)
                        self.writer.add_scalar(
                            'Validation Actual loss avg', loss.item(), turns_till_ten)
                        self.writer.add_histogram('Validation accurancy hist', torch.mean(
                            equals.type(torch.FloatTensor)).item(), turns_till_ten)
                        self.writer.add_histogram(
                            'Validation Actual loss avg hist', loss.item(), turns_till_ten)
                        turns_till_ten += 1
                        ###################################################################
                        # To compare with plt
                        self.val_loss.append(loss.item())
                        ###################################################################

                ###################################################################
                # Print the progress of our validation last position to check
                # End of the validation set
                if counter == len(self.validation_loader):
                    validation_accuracy = val_accuracy / counter
                    # print(counter, "/", len(self.validation_loader))
                    # https://thepythonguru.com/python-string-formatting/
                    print('Validation loss SAME than running loss {1:.6f} \t avg loss {2:.6f} \t last loss {3:.6f}'.format(
                        val_loss, val_running_loss, val_running_loss / counter, loss.item()))
                    print('correct guesses {0:d} from {1:d}'.format(
                        int(number_of_correct_elements), len(self.validation_loader.dataset)))
                    print('Validation accuracy {0:.2f}'.format(
                        validation_accuracy))
                    print('Time to validate the model was {0:.2f} seconds'.format(
                        time() - start_validation))
                    print()
                    # SAVING
                    self.savingArrays()

                counter += 1

    def finalTest(self):
        pass

    ##########################################################################
    # TBoard
    def addGraph(self, images):
        self.writer.add_graph(self.model, images)

    def addImages(self, images, count):
        # ? get a source
        # *
        # grid for tboard
        grid_img = self.loader.getGrid(opt.batch_size, images)
        self.writer.add_image('images', grid_img, count)

    def closeTBoard(self):
        '''
            close stream
            ! something happens [link](www.google.com)
        '''
        # ! must be handeled differently
        if opt.tboard:
            self.writer.close()

    ##########################################################################
    # Json file TODO
    def jsonFileActions(self, path_of_sjon_file: str, flag: str):
        '''
            jsonFileActions
        '''
        if flag == 'r':
            with open(path_of_sjon_file, "r") as read_file:
                data = json.load(read_file)
        elif flag == 'w':
            with open(path_of_sjon_file, 'w', encoding='utf-8') as write_file:
                json.dump(data, write_file, ensure_ascii=False, indent=4)

    def howLongIsTheClassRunningRightNow(self) -> time:
        '''
            return working hours of class
        '''
        print('How many seconds is this class running')
        print(time() - self.time_right_before_init)
        return time() - self.time_right_before_init

    def saveNetwork(self, epochs, loss):
        '''
            saveNetwork ./public/model/data
        '''
        print('Saving model')
        # todo
        path = './public/model/data'
        dest_folder = os.path.join(path, opt.problem)
        if not os.path.isdir(dest_folder):
            os.mkdir(dest_folder)
        model_name = self.new_path_to_create_folder_structure_to_save_results + '.pt'
        path_to_save_it = os.path.join(dest_folder, model_name)
        torch.save({
            'epoch': epochs,
            'model_state_dict': self.model.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
            'loss': loss
        }, path_to_save_it)

    def loadingModel(self, path_of_model: str):
        '''
            loadingModel
        '''
        if not self.model:
            self.model = self.getNetwork()
        # Model class must be defined somewhere
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        # Move model to the device specified above
        self.model.load_state_dict(torch.load(
            path_of_model, map_location=device))

        self.model.to(device)

    def drawingToCompareLosses(self):
        '''
            plot calculating freezing point for next model creation
        '''
        jump = False
        # ? idea old vs new
        # old_v_fct, old_t_fct = self.loadArrays()
        print(self.train_loss)
        print(self.val_loss)

        # freezing pts STOPPING
        intersection_pts = []
        if not jump:
            if len(self.train_loss) != len(self.val_loss):
                raise Exception('not the same')
            v_fct = np.asarray(self.val_loss)
            t_fct = np.asarray(self.train_loss)

            middle_fct = (t_fct + v_fct) / 2
            plt.xlabel('Length of dataset / batchsize')
            plt.ylabel('Losses of train and val')
            plt.title('Compare both loss arrays with each other')
            x_axis = [x for x in range(len(self.val_loss))]
            idx = np.argwhere(np.diff(np.sign(t_fct - v_fct))
                              != 0).reshape(-1) + 0
            plt.plot(x_axis, t_fct, 'g-')
            plt.plot(x_axis, v_fct, 'y-')
            plt.plot(x_axis, middle_fct, 'r-')
            # Interpolation
            for i in range(len(idx)):
                # plt.plot((x_axis[idx[i]]+x_axis[idx[i]+1])/2.,
                #          (t_fct[idx[i]]+t_fct[idx[i]+1])/2., 'bso')
                # ! freeze points for the model
                intersection_pts.append((x_axis[idx[i]]+x_axis[idx[i]+1])/2)
                plt.plot((x_axis[idx[i]]+x_axis[idx[i]+1])/2., (t_fct[idx[i]] +
                                                                t_fct[idx[i]+1]+v_fct[idx[i]]+v_fct[idx[i]+1])/4., 'ro')
            # plt.plot(v_fct[idx], t_fct[idx], 'ro')
            plt.legend(['train loss', 'vall loss', 'middle_fct'])
            plt.show()

    def savingArrays(self):
        '''
            savingArrays
        '''
        print(len(self.train_loss))
        print(len(self.val_loss))
        new_path_name = os.path.join(
            './np_arrays', self.new_path_to_create_folder_structure_to_save_results)

        if not os.path.isdir(new_path_name):
            os.makedirs(new_path_name)
        # ? join maybe better
        np.save(new_path_name + '/val_loss', self.val_loss)
        np.save(new_path_name + '/train_loss', self.train_loss)

    def loadArrays(self, path: str):
        '''
            loadArrays
        '''
        # todo redo
        # ! does not work only testing
        return np.load('np_arrays\\val_loss.npy'),  np.load('np_arrays\\train_loss.npy')

    def creatingTBoardFoldersForTheEvents(self, path_for_tb_data: str) -> str:
        '''
            creating tboard
        '''
        new_path_name = os.path.join(
            path_for_tb_data, self.new_path_to_create_folder_structure_to_save_results)

        if not os.path.isdir(new_path_name):
            os.makedirs(new_path_name)
        else:
            raise Exception('Already exist')

        return new_path_name

    def creatingFolderPathToSaveReults(self) -> str:
        '''
            creatingFolderPathToSaveReults -> str
        '''
        input_shape = str(
            opt.input_shape[0]) + str(opt.input_shape[1]) + str(opt.input_shape[2])
        new_path_name = ''
        list_of_items = [opt.cnn_name, opt.optimizer, input_shape, str(
            opt.lr), str(opt.epochs), str(opt.batch_size)]
        for idx in range(len(list_of_items)):
            new_path_name += list_of_items[idx]
            if len(list_of_items) != idx + 1:
                new_path_name += '_'

        return new_path_name


if __name__ == "__main__":
    crtl = Controller()

    crtl.closeTBoard()

    # * just for the first time to visualize it
    # crtl.drawingToCompareLosses()
    
    crtl.howLongIsTheClassRunningRightNow()
