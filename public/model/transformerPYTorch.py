import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision
import torchvision.datasets as datasets
import torchvision.models as models
import torchvision.transforms as transforms
from PIL import Image

import os


class TransformerPYTorch():
    '''
        data transformation with pytorch
    '''

    def __init__(self):
        pass

    def process_image(self, image_path: str = '', pixel_w: int = 224, pixel_h: int = 224, three_dimension=True) -> torch.Tensor:
        '''
            Process our image
            return <class 'torch.Tensor'>
        '''
        pixel_tuple = (pixel_w, pixel_h)

        # Load Image
        img = Image.open(image_path)
        img = img.resize(pixel_tuple)

        # # Turn image into numpy array
        numpy_array = np.array(img)

        if three_dimension:
            # Make the color channel dimension first instead of last
            numpy_array = numpy_array.transpose((2, 0, 1))
        else:
            # TODO try_out
            pass

        # Make all values between 0 and 1
        numpy_array = numpy_array/255

        # Normalize based on the preset mean and standard deviation stats in JSON files eg above
        # 0.5025086402893066, 0.477150559425354, 0.4313367009162903 => mean
        # 0.24657383561134338, 0.24676746129989624, 0.24634884297847748 => std
        numpy_array[0] = (numpy_array[0] - 0.5025086402893066) / \
            0.24657383561134338
        numpy_array[1] = (numpy_array[1] - 0.477150559425354) / \
            0.24676746129989624
        numpy_array[2] = (numpy_array[2] - 0.4313367009162903) / \
            0.24634884297847748

        # Add a fourth dimension to the beginning to indicate batch size
        numpy_array = numpy_array[np.newaxis, :]

        # Turn into a torch tensor
        torch_tensor = torch.from_numpy(numpy_array)
        torch_tensor = torch_tensor.float()

        return torch_tensor

    def dataTransformer(self, pixel_w: int, pixel_h: int, channels: int, path_of_data: str, batch_size: int = 32, mean: list = None, std: list = None) -> torch.utils.data.dataloader.DataLoader:
        '''
            DataLoader
        '''
        print('Loading data ...')
        print()
        
        if channels == 1:
            if True:
                _mean = [0.45340657]
                _std = [0.21607223]
            else:
                _mean = mean
                _std = std

            transformations = transforms.Compose([
                transforms.Resize((pixel_w, pixel_h)),
                transforms.Grayscale(num_output_channels=1),
                transforms.ToTensor(),
                # first calculate the mean and std
                transforms.Normalize(mean=_mean, std=_std)
            ])
        else:
            # RGB MODE
            if True:
                # [0.5664917 0.568451  0.5727116] [0.197387   0.19776866 0.197747  ]
                _mean = [0.5664917, 0.568451, 0.5727116]
                _std = [0.197387, 0.19776866, 0.197747]
            else:
                _mean = mean
                _std = std

            transformations = transforms.Compose([
                transforms.Resize((pixel_w, pixel_h)),
                transforms.ToTensor(),
                # first calculate the mean and std
                transforms.Normalize(mean=_mean, std=_std)
            ])

        # Load in each dataset and apply transformations using
        # the torchvision.datasets as datasets library
        data_set = datasets.ImageFolder(
            path_of_data, transform=transformations)

        data_loader = torch.utils.data.DataLoader(
            data_set, batch_size=batch_size, shuffle=True)

        return data_loader

    def showGrid(self, batch_size: int, images):
        # Create a grid with all images.
        nrwo = batch_size // 8
        grid_img = torchvision.utils.make_grid(images, nrow=nrwo)

        # print(grid_img.dtype)
        # print(grid_img.shape)

        # plt.imshow(grid_img.permute(1, 2, 0))
        # plt.show()

        return grid_img

    def getGrid(self, batch_size: int, images) -> torch.Tensor:
        # Create a grid with all images.
        nrwo = batch_size // 8
        grid_img = torchvision.utils.make_grid(images, nrow=nrwo)

        return grid_img

    def getIterator(self, data_loader): # -> torch.utils.data.dataloader._DataLoaderIter:
        return iter(data_loader)

    def nextIterationForDataset(self, data_iteration) -> torch.Tensor:
        images, labels = next(data_iteration)
        return images, labels
