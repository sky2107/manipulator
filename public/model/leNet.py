import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from PIL import Image


class LeNet(nn.Module):
    '''
        input_size = n  * n * c
        Rule for next amount of parameters in the next layer
        output_size * (input_size + 1) == number_parameters

        input_size = n  * n * c
        Rule for next amount of parameters in the next layer in CNN
        window_size == filter_size
        output_channels * (input_channels * window_size + 1) == number_parameters

        w, h => pixels in this case 32, 32
        \[ ((w + 2 * p - f)) / s ) + 1 * ((h + 2 * p - f)) / s ) + 1 = output \]

        plus w,h does not change if with same padding IMPORTANT
    '''

    def __init__(self, number_of_classes=10, input_shape=(240, 240, 3), out_conv1=6, out_conv2=16, out_fc1=120, out_fc2=84, init='Kaiming', pooling='average', dropout=False, p_dropout=0.2):
        self.number_of_classes = number_of_classes
        # He Kaiming Xavier
        self.init = init
        # cnn techniques
        self.pooling_technique = pooling
        self.p_dropout = p_dropout
        self.dropout = dropout

        # filter not necessary
        f=3
        same = self.samePadding(f)
        p = same
        # TODO even not modulo 10 MUST
        w = input_shape[0]
        h = input_shape[1]
        input_dim = input_shape[2]
        print('input_shape')
        print(w, h, input_dim)
        # learning_parameters == lp
        self.lp_conv1 = (w * h * input_dim * f + 1) * out_conv1

        super(LeNet, self).__init__()
        ########################################################################
        # w,h does not change if with same padding IMPORTANT diff than AlexNet eg
        ########################################################################
        # same padding p  = ( f - 1 ) / 2 otherwise valid padding
        self.conv1 = nn.Conv2d(input_dim, out_conv1,
                               kernel_size=f, padding=same)
        # same padding and stride=1
        w, h = self.sumOfConv(w, h)
        self.lp_conv2 = (w * h * out_conv1 * f + 1) * out_conv2
        self.conv2 = nn.Conv2d(out_conv1, out_conv2,
                               kernel_size=f, padding=same)
        w, h = self.sumOfConv(w, h)

        para_flatten = int(w * h * out_conv2)
        print(para_flatten)
        # learning_parameters == lp
        self.lp_fc1 = out_fc1 * (para_flatten + 1)
        self.lp_fc2 = out_fc2 * (out_fc1 + 1)
        print(self.lp_fc2)
        self.lp_fc3 = self.number_of_classes * (out_fc2 + 1)

        self.conv2_drop = nn.Dropout2d(p=self.p_dropout)  # , training=True
        self.fc1 = nn.Linear(para_flatten, out_fc1)
        self.fc2 = nn.Linear(out_fc1, out_fc2)
        self.fc3 = nn.Linear(out_fc2, self.number_of_classes)

        # Find the device available to use using torch library
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.learning_parameters_amount()

    def forward(self, x):
        # Computes the activation of the first convolution
        # Size changes from (3, 32, 32) to (6, 16, 16)
        # stride is default avg_pool 2 padding 0
        if self.pooling_technique == 'average':
            # Input layer
            x = F.relu(F.avg_pool2d(self.conv1(x), (2,)))
            # hidden layer
            x = F.relu(F.avg_pool2d(self.conv2(x), (2,)))
        # Size changes from (6, 16, 16) to (16, 8, 8)
        elif self.pooling_technique == 'max':
            # Input layer
            x = F.relu(F.max_pool2d(self.conv1(x), (2,)))
            # hidden layer
            x = F.relu(F.max_pool2d(self.conv2(x), (2,)))
        # 16 * 8 * 8 = 1024
        if self.dropout:
            x = self.conv2_drop(x)

        x = x.view(-1, self.flatten(x))
        # hidden layer
        x = F.relu(self.fc1(x))
        # hidden layer
        x = F.relu(self.fc2(x))
        # Output layer
        x = self.fc3(x)
        # LOG is better
        return F.log_softmax(x, dim=1)

    def flatten(self, x):
        num_features = 1
        for s in x.size()[1:]:
            num_features *= s
        self.num_features = num_features
        return num_features

    def sumOfConv(self, w, h, padding=0, _filter=2, stride=2):
        w = ((w + 2 * padding - _filter) // stride) + 1
        h = ((h + 2 * padding - _filter) // stride) + 1
        if self.pooling_technique == 'average':
            print('After Avg. Pooling:')
        else:
            print('After Max. Pooling:')
        print(w, h)
        print('######################')
        return w, h

    def init_kaimingHE(self, m):
        '''
            all the weights plus bias zero
            no he
            The He initialization performance better than the Xavier initialization 
            Kaiming only by leaky
            NORMALLY HE fct
            if 'bogiot' == method_of_init:
                method_for_w_to_init = 'he_uniform'
            elif 'hinton' == method_of_init:
                method_for_w_to_init = 'he_normal'
        '''
        if type(m) == nn.Linear:
            if self.init == 'Kaiming':
                nn.init.kaiming_uniform_(m.weight)
            elif self.init == 'Xavier':
                nn.init.xavier_uniform_(m.weight)
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif type(m) == nn.Conv2d:
            if self.init == 'Kaiming':
                nn.init.kaiming_uniform_(m.weight)
            elif self.init == 'Xavier':
                nn.init.xavier_uniform_(m.weight)
            if m.bias is not None:
                # nn.init.kaiming_uniform_(m.bias)
                nn.init.constant_(m.bias, 0)

    def learning_parameters_amount(self):
        lp_sum = self.lp_conv1 + self.lp_conv2 + \
            self.lp_fc1 + self.lp_fc2 + self.lp_fc3
        print(lp_sum)
        return lp_sum

    def samePadding(self, f):
        print(type(f))
        return (f - 1) // 2

    def predict(self, torch_tensor):
        '''
            before process torch_tensor to torch.float32
        '''
        if torch_tensor.dtype == torch.float32:
            # Pass the image through our model
            torch_tensor = torch_tensor.to(self.device)

            output = self.forward(torch_tensor)

            # Reverse the log function in our output
            output = torch.exp(output)

            # Get the top predicted class, and the output percentage for
            # that class
            probs, classes = output.topk(1, dim=1)
            return probs.item(), classes.item()
        else:
            return None
