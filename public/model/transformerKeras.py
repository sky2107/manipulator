from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf

import pathlib
import os

import random


print(tf.compat.v1.enable_eager_execution())
print(tf.__version__)

# ? for what
AUTOTUNE = tf.data.experimental.AUTOTUNE


class LoaderKeras:
    """

    """

    def __init__(
        self,
        path_of_data: str,
        pixel_w: int = 200,
        pixel_h: int = 200,
        channel: int = 3,
        batch_size: int = 32,
    ):
        self.path_of_data = path_of_data

        self.pixel_w = pixel_w
        self.pixel_h = pixel_h
        self.channel = channel

        self.batch_size = batch_size
        # self.all_labels = self.getAllLabels(path_of_data)
        # self.all_images = self.allImagesWithLabelsAsKey()

        # self.count()
        self.image_label_ds, self.image_ds, self.label_ds = self.dtLoader(path_of_data)
        self.ds = self.dtLoaderVersionTWO(path_of_data)

        data_root = pathlib.Path(path_of_data)
        all_image_paths = list(data_root.glob("*/*"))
        all_image_paths = [str(path) for path in all_image_paths]
        self.steps_per_epoch = tf.math.ceil(
            len(all_image_paths) / self.batch_size
        ).numpy()

    def getAllLabels(self, path_of_data: str):
        """

        """
        return [
            o
            for o in os.listdir(path_of_data)
            if os.path.isdir(os.path.join(path_of_data, o))
        ]

    def allImagesWithLabelsAsKey(self):
        """

        """
        all_images = {}
        for label in self.all_labels:
            path_of_label_data = os.path.join(self.path_of_data, label)
            # all_images[label] = [f for r, d, f in os.walk(path_of_label_data)]
            list = []
            for root, dirs, files in os.walk(path_of_label_data, topdown=False):
                for name in files:
                    if name.endswith(".jpg"):
                        list.append(os.path.join(root, name))
            all_images[label] = list

        return all_images

    def count(self):
        for label in self.all_labels:
            print("Label => ", label)
            print(len(self.all_images[label]))

    def preprocess_image(self, image):
        image = tf.image.decode_jpeg(image, channels=self.channel)
        image = tf.image.resize(image, [self.pixel_w, self.pixel_h])
        image /= 255.0  # normalize to [0,1] range

        return image

    def load_and_preprocess_image(self, path):
        image = tf.io.read_file(path)
        return self.preprocess_image(image)

    def load_and_preprocess_from_path_label(self, path, label):
        return self.load_and_preprocess_image(path), label

    def dtLoader(self, path_of_data):
        """
            tf.data
        """
        data_root = pathlib.Path(path_of_data)
        all_image_paths = list(data_root.glob("*/*"))
        all_image_paths = [str(path) for path in all_image_paths]

        random.shuffle(all_image_paths)

        image_count = len(all_image_paths)

        label_names = sorted(
            item.name for item in data_root.glob("*/") if item.is_dir()
        )

        label_to_index = dict((name, index) for index, name in enumerate(label_names))

        all_image_labels = [
            label_to_index[pathlib.Path(path).parent.name] for path in all_image_paths
        ]

        path_ds = tf.data.Dataset.from_tensor_slices(all_image_paths)

        image_ds = path_ds.map(
            self.load_and_preprocess_image, num_parallel_calls=AUTOTUNE
        )

        label_ds = tf.data.Dataset.from_tensor_slices(
            tf.cast(all_image_labels, tf.int64)
        )

        image_label_ds = tf.data.Dataset.zip((image_ds, label_ds))
        print(image_label_ds)
        return image_label_ds, image_ds, label_ds

    def dtLoaderVersionTWO(self, path_of_data):
        """
            tf.data
        """
        data_root = pathlib.Path(path_of_data)
        all_image_paths = list(data_root.glob("*/*"))
        all_image_paths = [str(path) for path in all_image_paths]

        random.shuffle(all_image_paths)

        image_count = len(all_image_paths)

        label_names = sorted(
            item.name for item in data_root.glob("*/") if item.is_dir()
        )

        label_to_index = dict((name, index) for index, name in enumerate(label_names))

        all_image_labels = [
            label_to_index[pathlib.Path(path).parent.name] for path in all_image_paths
        ]
        ds = tf.data.Dataset.from_tensor_slices((all_image_paths, all_image_labels))
        image_label_ds = ds.map(self.load_and_preprocess_from_path_label)
        # print(image_label_ds)
        BATCH_SIZE = 32

        # Setting a shuffle buffer size as large as the dataset ensures that the data is
        # completely shuffled.
        ds = image_label_ds.shuffle(buffer_size=image_count)
        ds = ds.repeat()
        ds = ds.batch(BATCH_SIZE)
        # `prefetch` lets the dataset fetch batches, in the background while the model is training.
        ds = ds.prefetch(buffer_size=AUTOTUNE)
        # print(ds)
        return ds

    def change_range(self, image, label):
        return 2 * image - 1, label


if __name__ == "__main__":
    path = "C:\\Users\\FelixNavas\\Documents\\Manipulator\\data\\screenshots"
    loader = LoaderKeras(path)
