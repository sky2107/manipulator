# Networks from torchvsion

> Pretained networks <br>
> torch.utils.model_zoo https://pytorch.org/docs/0.4.0/model_zoo.html

1. AlexNet 
2. VGG
3. ResNet
4. SqueezeNet
5. DenseNet
6. Inception v3

> https://pytorch.org/docs/0.4.0/torchvision/models.html#id1
