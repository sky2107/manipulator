import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from PIL import Image


class AlexNet(nn.Module):
    '''
        AlexNet
    '''

    def __init__(self, number_of_classes=8, input_shape=(240, 240, 3), out_conv1=96, out_conv2=256, out_conv3=384,
                 out_conv4=256, out_fc1=512, out_fc2=256, init='Kaiming', pooling='max', dropout=True, p_dropout=0.2):
        self.number_of_classes = number_of_classes
        # He Kaiming Xavier
        self.init = init
        # cnn techniques
        self.pooling_technique = pooling
        self.p_dropout = p_dropout
        self.dropout = dropout

        # filter parameter
        f = 11

        w = input_shape[0]
        h = input_shape[1]
        input_dim = input_shape[2]
        print('input_shape')
        print(w, h, input_dim)
        # learning_parameters == lp
        self.lp_conv1 = (w * h * input_dim * f + 1) * out_conv1

        super(AlexNet, self).__init__()
        # same padding p  = ( f - 1 ) / 2 otherwise valid padding
        self.conv1 = nn.Conv2d(input_dim, out_conv1,
                               kernel_size=11, padding=self.samePadding(11), stride=4)
        w, h = self.convAlgo(w, h, 11, 4)
        w, h = self.sumOfConv(w, h)
        f = 5
        self.lp_conv2 = (w * h * out_conv1 * f + 1) * out_conv2
        ##################################################################
        self.conv2 = nn.Conv2d(out_conv1, out_conv2,
                               kernel_size=5, padding=self.samePadding(5), stride=1)
        w, h = self.convAlgo(w, h, 5, 1)
        w, h = self.sumOfConv(w, h)
        f = 3
        self.lp_conv3 = (w * h * out_conv2 * f + 1) * out_conv3
        ##################################################################
        self.conv3 = nn.Conv2d(out_conv2, out_conv3,
                               kernel_size=3, padding=self.samePadding(3))
        w, h = self.convAlgo(w, h, 3, 1)

        self.lp_conv4 = (w * h * out_conv3 * f + 1) * out_conv4
        ##################################################################
        self.conv4 = nn.Conv2d(out_conv3, out_conv4,
                               kernel_size=3, padding=self.samePadding(3))
        w, h = self.convAlgo(w, h, 3, 1)
        w, h = self.sumOfConv(w, h)
        print('pixel')
        print(w)
        print(h)
        print(out_conv4)
        ##################################################################
        para_flatten = int(w * h * out_conv4)
        # learning_parameters == lp
        self.lp_fc1 = out_fc1 * (para_flatten + 1)
        self.lp_fc2 = out_fc2 * (out_fc1 + 1)
        self.lp_fc3 = self.number_of_classes * (out_fc2 + 1)
        self.drop = nn.Dropout(p=self.p_dropout)
        print('Connection to fc')

        print(para_flatten)
        self.fc1 = nn.Linear(para_flatten, out_fc1)
        self.fc2 = nn.Linear(out_fc1, out_fc2)
        self.fc3 = nn.Linear(out_fc2, self.number_of_classes)

        # Find the device available to use using torch library
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu")

        self.learning_parameters_amount()

    def forward(self, x):
        # print(self.pooling_technique)
        # Computes the activation of the first convolution
        # stride is default avg_pool 2 padding 0
        if self.pooling_technique == 'average':
            # print('Features Pooling avg')
            x = F.relu(self.conv1(x))
            x = F.avg_pool2d(x, (3, 2))
            x = F.relu(self.conv2(x))
            x = F.avg_pool2d(x, (3, 2))
            x = F.relu((self.conv3(x)))
            x = F.relu(self.conv4(x))
            # print(x.size())
            x = F.avg_pool2d(x, (3, 2))
            # print(x.size())
        # Size changes from (6, 16, 16) to (16, 8, 8)
        elif self.pooling_technique == 'max':
            # print('Features Pooling max')
            x = F.relu(self.conv1(x))
            x = F.max_pool2d(x, (3, 2))
            x = F.relu(self.conv2(x))
            x = F.max_pool2d(x, (3, 2))
            x = F.relu((self.conv3(x)))
            x = F.relu(self.conv4(x))
            # print(x.size())
            x = F.max_pool2d(x, (3, 2))
            # print(x.size())
        x = x.view(-1, self.flatten(x))
        # print(x.size())
        x = self.drop(F.relu(self.fc1(x)))
        x = self.drop(F.relu(self.fc2(x)))
        x = self.fc3(x)
        # LOG is better
        return F.log_softmax(x, dim=1)

    def flatten(self, x):
        num_features = 1
        for s in x.size()[1:]:
            num_features *= s
        return num_features

    def convAlgo(self, w, h, f, stride=2, same=True):
        if True:
        # if stride == 1 and same:
        #     # nothing changes with same padding
        #     pass
        # else:
            w = ((w + 2 * self.samePadding(f) - f) // stride) + 1
            h = ((h + 2 * self.samePadding(f) - f) // stride) + 1
            print('pixel w and h:')
            print(w, h)
            print('######################')
        return w, h

    def sumOfConv(self, w, h, padding=0, _filter=(3, 2), stride=(3, 2)):
        '''
            LIMIT 1,3
            w = (([1...5] - 3) // 3) + 1
            h = ((2 - 2) // 2) + 1
            -1 // 3 = -1 int devision
            (-1) // 2 = -1
            LIMITS w > 3 and h > 2
        '''
        print('Before ', w, h)
        new_w = ((w - _filter[0]) // stride[0]) + 1
        new_h = ((h - _filter[1]) // stride[1]) + 1
        print('NEW')
        print(new_w, new_h)
        if new_w > 0:
            # 2 * padding = 0
            # new_w = ((w + 2 * padding - _filter[0]) // stride[0]) + 1
            w = new_w
        elif w < 6:
            w = 1

        if new_h < 2:
            h = 1
        else:
            h = new_h
        print('After Pooling: ', self.pooling_technique)
        print(w, h)
        print('######################')
        return w, h

    def init_kaimingHE(self, m):
        '''
            all the weights plus bias zero
            no he
            The He initialization performance better than the Xavier initialization
            Kaiming only by leaky
            NORMALLY HE fct
            if 'bogiot' == method_of_init:
                method_for_w_to_init = 'he_uniform'
            elif 'hinton' == method_of_init:
                method_for_w_to_init = 'he_normal'
        '''
        if type(m) == nn.Linear:
            if self.init == 'Kaiming':
                nn.init.kaiming_uniform_(m.weight)
            elif self.init == 'Xavier':
                nn.init.xavier_uniform_(m.weight)
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif type(m) == nn.Conv2d:
            if self.init == 'Kaiming':
                nn.init.kaiming_uniform_(m.weight)
            elif self.init == 'Xavier':
                nn.init.xavier_uniform_(m.weight)
            if m.bias is not None:
                # nn.init.kaiming_uniform_(m.bias)
                nn.init.constant_(m.bias, 0)

    def samePadding(self, f):
        return ((int(f) - 1) // 2)

    def learning_parameters_amount(self):
        lp_sum = self.lp_conv1 + self.lp_conv2 + self.lp_conv3 + \
            self.lp_conv4 + self.lp_fc1 + self.lp_fc2 + self.lp_fc3
        print('LP: ', lp_sum)
        return lp_sum

    def predict(self, torch_tensor):
        '''
            before process torch_tensor to torch.float32
        '''
        if torch_tensor.dtype == torch.float32:
            # Pass the image through our model
            torch_tensor = torch_tensor.to(self.device)

            output = self.forward(torch_tensor)

            # Reverse the log function in our output
            output = torch.exp(output)

            # Get the top predicted class, and the output percentage for
            # that class
            probs, classes = output.topk(1, dim=1)
            return probs.item(), classes.item()
        else:
            return None
