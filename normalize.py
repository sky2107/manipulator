# tensorboard with pytorch
# Import Libraries
# from torch.utils.tensorboard import SummaryWriter
import json
import timeit

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.datasets as datasets
import torchvision.models as models
import torchvision.transforms as transforms
from PIL import Image

torch.set_printoptions(linewidth=120)
torch.set_grad_enabled(True)

print(torch.__version__)
print(torchvision.__version__)


class Normalize:

    def __init__(self, path_of_data: str, batch_size: int, cross_validation_is_used: bool = False):
        self.cross_validation_is_used = cross_validation_is_used
        self.path_of_data = path_of_data
        self.batch_size = batch_size
        ######################################################
        self.datasets = {}
        self.list_with_all_stats = []
        # SETUP
        setup_exe = True
        if setup_exe:
            self.setUp()

        if self.cross_validation_is_used:
            self.whichDataset = path_of_data.split('/')[-1]
            self.whichDataset = self.whichDataset.split('_')[0]

            self.current_datasets = []
            if self.whichDataset == 'fifty':
                number = 50
            elif self.whichDataset == 'thirty':
                number = 30
            elif self.whichDataset == 'ten':
                number = 10
            else:
                number = 80

            self.current_datasets = self.datasets[number]
            self.percentage_of_ds = number

            print(self.current_datasets)

    def normalizeAvgOrMinMax(self, option: str, channels: int = 3) -> list:
        '''
            option avg, min or max
        '''
        if option == 'avg':
            key = "Average img size"
        if option == 'min':
            key = "Min image"
        elif option == 'max':
            key = "Max image"

        print('######################################')
        x, y = self.current_datasets[key]
        print(x, y, key)
        mean, std = self.calculateNormalization(
            x, y, channels, self.path_of_data, self.batch_size)
        return mean, std

    def calculateNormalization(self, pixel_w: int, pixel_h: int, channels: int, path_of_data: str, batch_size: int = 32) -> list:
        if channels == 1:
            transformations = transforms.Compose([
                transforms.Resize((pixel_w, pixel_h)),
                transforms.Grayscale(num_output_channels=channels),
                transforms.ToTensor()
            ])
        else:
            transformations = transforms.Compose([
                transforms.Resize((pixel_w, pixel_h)),
                transforms.ToTensor()
            ])

        # Load in each dataset and apply transformations using
        # the torchvision.datasets as datasets library
        train_set = datasets.ImageFolder(
            path_of_data, transform=transformations)

        train_loader = torch.utils.data.DataLoader(
            train_set, batch_size=batch_size, shuffle=True)

        # batch size doesn’t matter the generator resamples
        images, labels = iter(train_loader).next()

        show_images_for_grid = False
        if show_images_for_grid:
            # Create a grid with all images.
            nrwo = batch_size // 8
            grid_img = torchvision.utils.make_grid(images, nrow=nrwo)

            print(grid_img.dtype)
            print(grid_img.shape)

            plt.imshow(grid_img.permute(1, 2, 0))
            plt.show()

        # images.shape = ( 32, 3, 80, 80)
        print(images.shape)
        numpy_images = images.numpy()

        per_image_mean = np.mean(numpy_images, axis=(2, 3))  # Shape (32,3)
        per_image_std = np.std(numpy_images, axis=(2, 3))  # Shape (32,3)

        pop_channel_mean = np.mean(per_image_mean, axis=0)  # Shape (3,)
        pop_channel_std = np.mean(per_image_std, axis=0)  # Shape (3,)
        # print(per_image_mean, per_image_std)
        print(pop_channel_mean, pop_channel_std)

        return pop_channel_mean, pop_channel_std

    def readJsonfile(self, path_of_json_file: str) -> list:
        '''
            return json file type list
        '''
        with open(path_of_json_file, "r") as read_file:
            data = json.load(read_file)

        return data

    def writeJsonFile(self, data: dict, path_for_json: str):
        with open(path_for_json, 'w', encoding='utf-8') as write_file:
            json.dump(data, write_file, ensure_ascii=False, indent=4)

    def getAllInformationAboutDatasetsInJsonFile(self, data: list) -> list:
        keys = [keys for keys in data]
        self.datasets[80] = (data[keys[0]])
        self.datasets[50] = (data[keys[1]])
        self.datasets[30] = (data[keys[2]])
        self.datasets[10] = (data[keys[3]])
        print('###############################################')
        print(keys)
        arr = [data[k] for k in keys]
        list_with_all_stats = []
        dict_with_all_stats = {}
        index = 0
        for e in arr:
            newKeys = [k for k in e]
            li = [e[k] for k in newKeys]
            list_with_all_stats.append(li)
            dict_with_all_stats[keys[index]] = li
            index += 1

        print(dict_with_all_stats)
        print('###############################################')
        print(list_with_all_stats)
        self.list_with_all_stats = list_with_all_stats
        return keys, list_with_all_stats

    def setUp(self):
        '''
            setup
        '''
        # TODO ISSUE
        # path_of_json_file = './json/natural_images/statsOfDatasets_data_augmentation.json'
        path_of_json_file = './statsOfDatasets.json'
        data = self.readJsonfile(path_of_json_file)
        if self.cross_validation_is_used:
            self.getAllInformationAboutDatasetsInJsonFile(data)

    def writeIntoJsonFile(self):
        jsonFile = []
        newDic = {}
        options = ['avg', 'min', 'max']
        # 1 or 3 channels
        channels = 1
        for opt in options:
            mean, std = self.normalizeAvgOrMinMax(opt, channels=channels)
            newDic[opt] = (mean.tolist(), std.tolist())

        dicti_ds = {}
        dicti_ds[self.percentage_of_ds] = newDic
        path_of_json = './json/natural_images/' + str(self.percentage_of_ds) + '/normalization_batch_size_' + str(self.batch_size) + '_channels_' + str(channels) + '.json'
        self.writeJsonFile(dicti_ds, path_of_json)

if __name__ == "__main__":

    # path_of_data = './data_augmentation/natural_images'
    # path_of_data = './data_augmentation/fifty_percent_of_natural_images'
    # path_of_data = './data_augmentation/thirty_percent_of_natural_images'
    # path_of_data = './data_augmentation/ten_percent_of_natural_images'

    path_of_data = './data/screenshots'
    # 32, 64, 128
    batch_size = 32
    normi = Normalize(path_of_data, batch_size)

    # normi.writeIntoJsonFile()

    pixel_w = 30
    pixel_h = 30
    channels = 1
    normi.calculateNormalization(pixel_w, pixel_h, channels, path_of_data, batch_size)