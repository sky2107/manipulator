#!/usr/bin/python3

import glob
import json
import os
import random
import shutil
import sys
import timeit
from shutil import copyfile, rmtree
from zipfile import ZipFile
from tqdm import tqdm

from PIL import Image, ImageFilter


class Manipulator:
    """
        folder: str = '', fromScratch: bool = False, cleanUpOnly: bool = False
        folder 'data_images/' MUST
        For the manipulation of the images
        to create more content
    """

    # -------------------------------------------------------------------------------
    # CONSTANTS
    QUANTITY_DATA_LEVEL = [100, 50, 10]
    # label change to train_data
    ALL_FOLDERNAMES = [
        "data",
        "gray_img",
        "blurred_img",
        "rotation_img",
        "mirroring_img",
        "crop_img",
    ]
    MANIPULATION_FOLDERNAMES = [
        "gray_img/",
        "blurred_img/",
        "rotation_img/",
        "mirroring_img/",
        "crop_img/",
        "contrast_img/",
    ]
    # crossvalidation
    # ? for screenshot data set ['_train/', '_val/'] # , '_test']
    TRAINING_VAL_DATA_FOLDERNAMES = ["_train/", "_val/"]  # , '_test']
    CROSSVAL_PERCENTAGE = [80, 10, 10]
    ROTATION_LEVEL_FOLDERNAMES = [
        "level_1/",
        "level_2/",
        "level_3/",
        "level_4/",
        "level_5/",
    ]
    IMAGE_FORMAT = ["jpg", "png"]
    ROTATION_ANGLES = [15, 30, 45, 90, 180]
    CROP_PERCENTAGE = [10, 20]
    CROP_METHOD = ["center", "left", "right"]

    def __init__(
        self, folder: str = "", fromScratch: bool = False, cleanUpOnly: bool = False
    ):
        if cleanUpOnly:
            self.cleanUpLabelStructure("./data")
            self.createFolderStructure("./data", False)
            return
        if fromScratch and not os.path.isdir("./" + folder):
            print("zip")
            # -------------------------------------------------------------------------------
            # zippi
            # self.extractDataFromZip()
            # -------------------------------------------------------------------------------
            self.extractDataFromZip(f"zip/{folder}.zip", target=f"./data/{folder}")
        folder = folder  # 'natural_images/'
        if folder == "":
            folder = "natural_images/"
        self.data_path = "./data/"
        # self.data_path = './data_augmentation'
        self.cross_folder = self.data_path + folder.split("/")[0]
        # -------------------------------------------------------------------------------
        self.path = self.data_path + folder
        self.gray_path = self.path + "gray_img/"
        self.blurred_path = self.path + "blurred_img/"
        # after the last folder 5 folder inthe dir named level_ + rotation_level
        self.rotation_path = self.path + "rotation_img/"
        self.mirroring_path = self.path + "mirroring_img/"
        # center 10 - 20 percent
        self.crop_path = self.path + "crop_img/"

        # New CODE data augmentation
        self.contrast_path = self.path + "contrast_img/"

        # './data/maninpulated_natural_images/'
        self.target_folder = self.data_path + "maninpulated_" + folder
        # -------------------------------------------------------------------------------
        # mainpulator folder
        # self.createFolderStructure(self.target_folder)
        # ['airplane', 'car', 'cat', 'dog', 'flower', 'fruit', 'motorbike', 'person']
        self.all_labels = self.getAllLabels_(self.path)
        # -------------------------------------------------------------------------------
        # creating manipulation folder
        # for fol in Manipulator.MANIPULATION_FOLDERNAMES:
        #     n_folder = self.target_folder + fol
        #     self.createFolderStructure(n_folder)
        #     for lab in self.all_labels:
        #         self.createFolderStructure(n_folder + lab)
        #         if fol == 'rotation_img/':
        #             for rot in Manipulator.ROTATION_LEVEL_FOLDERNAMES:
        #                 self.createFolderStructure(n_folder + lab + '/' + rot)
        #         elif fol == 'crop_img/':
        #             for rot in Manipulator.CROP_METHOD:
        #                 self.createFolderStructure(n_folder + lab + '/' + rot)
        #                 for cro_per in Manipulator.CROP_PERCENTAGE:
        #                     self.createFolderStructure(
        #                         n_folder + lab + '/' + rot + '/' + str(cro_per))
        # -------------------------------------------------------------------------------
        # endings
        self.jpg = ".jpg"
        self.png = ".png"
        # -------------------------------------------------------------------------------
        self.listOfImages = []
        print("Labeling")
        for label in self.all_labels:
            pth = self.path + label
            self.listOfImages.append(self.walk_through_data(pth, self.jpg))
        # -------------------------------------------------------------------------------
        # single image plus filename
        self.img = None
        self.filename = None
        # -------------------------------------------------------------------------------
        # rotation angels 5 levels 0 - 4
        self.rotation_angles = [15, 30, 45, 90, 180]
        # -------------------------------------------------------------------------------
        # cropping generalizable 10 - 20 percent
        self.crop_reduction_10_percent = None
        self.crop_reduction_20_percent = None
        # -------------------------------------------------------------------------------
        # Balancing data
        self.number_of_each_category = []
        self.max_item = 0
        self.amount_data = 0
        self.min_w = 10000
        self.min_h = 10000
        self.max_w = 0
        self.max_h = 0
        # both para above will be set after count_data
        self.count_data()
        # self.average_w, self.average_w = self.getAverageSizeofImages()
        self.average_w, self.average_w = self.getAverageSizeofImages(True)
        # -------------------------------------------------------------------------------
        # Balancing
        self.firstBalanceData()
        print(self.number_of_each_category)
        # -------------------------------------------------------------------------------
        # Generating duplicates
        # self.createMoreContentWithSameImage(200)
        # -------------------------------------------------------------------------------
        # Dividing data into 50/30/10
        # -------------------------------------------------------------------------------
        # self.files_unique = {}
        # self.fifty_percent_of_data_img_path = self.data_path + 'fifty_percent_of_' + folder
        # self.thirty_percent_of_data_img_path = self.data_path + 'thirty_percent_of_' + folder
        # self.ten_percent_of_data_img_path = self.data_path + 'ten_percent_of_' + folder

        # self.resizingDataSet(self.fifty_percent_of_data_img_path, 50)
        # self.resizingDataSet(self.thirty_percent_of_data_img_path, 30, flag=False)
        # self.resizingDataSet(self.ten_percent_of_data_img_path, 10, flag=False)
        # -------------------------------------------------------------------------------
        # Manipulations
        self.creatAllManipulations(mode=False)
        # self.blurrAllImages()
        # self.grayAllImages()
        # self.rotationOfAllElements()
        # self.mirrorAllImages()
        # self.cropAllImages()
        # -------------------------------------------------------------------------------
        # moving files cross validation 80/10/10
        # self.movingForCrossvalidation()

    # -------------------------------------------------------------------------------
    # Resizing 50 30 10 Percent with same files
    def resizingDataSet(self, path: str, resizing: int = 50, flag: bool = True):
        """
            Resizing 50 30 10 Percent with same files
            path: str, resizing: int = 50
            FIRST TIME FLAG MUST BE TRUE FOR THE 50% list of images
        """
        self.createFolderStructure(path, False)
        files = []
        index = 0
        # CARFUL 80 % of the data is now available
        print("Data resizing:", resizing)
        if resizing == 50:
            fifty_quantity = self.max_item // 8 * 5
            quantity = fifty_quantity
        elif resizing == 30:
            thirty_quantity = self.max_item // 8 * 3
            quantity = thirty_quantity
        else:
            ten_quantity = self.max_item // 8
            quantity = ten_quantity
        ####################################################
        # Orignal size
        ####################################################
        # if resizing == 50:
        #     fifty_quantity = self.max_item // 2
        #     quantity = fifty_quantity
        # elif resizing == 30:
        #     thirty_quantity = self.max_item // 10 * 3
        #     quantity = thirty_quantity
        # else:
        #     ten_quantity = self.max_item // 10
        #     quantity = ten_quantity
        ####################################################
        print(quantity)
        print("##########################")

        for lab in self.all_labels:
            ppath = path + lab
            origin_path = self.path + lab
            print("dir split", ppath)
            print("Origin path", origin_path)
            if not os.path.isdir(ppath):
                print("creating dir")
                self.createFolderStructure(ppath, False)

            for _ in range(quantity):
                if flag:
                    files.append(
                        random.choice(
                            [
                                x
                                for x in os.listdir(origin_path)
                                if os.path.isfile(os.path.join(origin_path, x))
                                and x not in files
                            ]
                        )
                    )
                else:
                    files.append(
                        random.choice(
                            [x for x in self.files_unique[lab] if x not in files]
                        )
                    )
            if flag:
                self.files_unique[lab] = files

            # than copy all files into original folder
            print("files resizing quantity")
            print(len(files))
            for idx, file in enumerate(files):
                new_filename = os.path.join(
                    ppath, file.split(".jpg")[0] + str(idx) + ".jpg"
                )
                self.copyfile(os.path.join(origin_path, file), new_filename)
            print("Filename unique")
            print(len(self.files_unique))
            print(type(self.files_unique))
            files = []
            index += 1

    def creatingTestData(self):
        """
           creating test Data
           https://github.com/tomahim/py-image-dataset-generator
        """
        pass

    # -------------------------------------------------------------------------------
    def movingForCrossvalidation(self):
        """
            cross validation 80/10/10 default
        """
        ten_percent = 10
        files = []
        # 80/10/10 maybe not dividable without REST
        val_test = (self.max_item // 100) * ten_percent  #  50 #
        eighty_percent = self.max_item - 2 * val_test  # 1000 #
        cross = [eighty_percent, val_test, val_test]  # , val_test]
        files = []
        index = 0
        for str_ in Manipulator.TRAINING_VAL_DATA_FOLDERNAMES:
            pth = self.cross_folder + str_
            self.createFolderStructure(pth)
            for lab in self.all_labels:
                dest_fol = os.path.join(pth, lab)
                src_fol = os.path.join(self.path, lab)
                self.createFolderStructure(dest_fol)
                for i in range(cross[index]):
                    files.append(
                        random.choice(
                            [
                                x
                                for x in os.listdir(src_fol)
                                if os.path.isfile(os.path.join(src_fol, x))
                                and x not in files
                            ]
                        )
                    )
                    src = os.path.join(src_fol, files[i])
                    dest = os.path.join(dest_fol, files[i])
                    self.moveFile(src, dest)
                files = []
            index += 1

    def moveFile(self, src, dest):
        """
            movingfiles
        """
        # both techniques are doing the same
        # os.rename(src, dest)
        shutil.move(src, dest)

    # -------------------------------------------------------------------------------

    def firstBalanceData(self):
        """
            doc
        """
        files = []
        index = 0
        # print('Folder images different:')
        for lab in self.all_labels:
            path = self.path + lab
            diff = self.max_item - self.number_of_each_category[index]
            # print(diff)
            for _ in range(diff):
                files.append(
                    random.choice(
                        [
                            x
                            for x in os.listdir(path)
                            if os.path.isfile(os.path.join(path, x)) and x not in files
                        ]
                    )
                )
            # than copy all files into original folder
            for idx, file in enumerate(files):
                new_filename = os.path.join(
                    path, file.split(".jpg")[0] + str(idx) + ".jpg"
                )
                self.copyfile(os.path.join(path, file), new_filename)

            files = []
            index += 1

    def createMoreContentWithSameImage(self, duplicates: int):
        """
            doc
        """
        print("Generating duplicates ", duplicates)
        files = []
        for lab in self.all_labels:
            path = self.path + lab
            for _ in range(duplicates):
                files.append(
                    random.choice(
                        [
                            os.path.join(path, x)
                            for x in os.listdir(path)
                            if os.path.isfile(os.path.join(path, x)) and x not in files
                        ]
                    )
                )
            print(len(files))
            # onlyfiles = [os.path.join(path, f) for f in os.listdir(
            #     path) if os.path.isfile(os.path.join(path, f))]
            # print(len(files))
            for idx, file in enumerate(files):
                print(file)
                new_filename = file.split(".jpg")[0] + str(idx) + ".jpg"
                print(new_filename)
                self.copyfile(file, new_filename)
            files = []

    def copyfile(self, src, dest):
        """
            doc
        """
        # both techniques are doing the same
        # os.rename(src, dest)
        copyfile(src, dest)

    def count_data(self):
        """
            counting data
        """
        print("Counting:")
        count = 0
        max_item = 0
        copies = 0
        dir_set = False
        # labels TODO
        folders = []
        for (root, dirs, files) in os.walk(self.path, topdown=True):
            if not dir_set:
                folders = dirs
                dir_set = True
            if len(files) > 0:
                self.number_of_each_category.append(len(files))
            if max_item < len(files):
                max_item = len(files)
            for file in files:
                if file.endswith(".jpg"):
                    count += 1
        self.amount_data = count
        self.max_item = max_item

    def getAverageSizeofImages(self, write_into_Json: bool = False):
        width = []
        height = []
        min_w = 10000
        min_h = 10000
        max_w = 0
        max_h = 0

        for (root, dirs, files) in os.walk(self.path, topdown=True):
            for file in files:
                if file.endswith(".jpg"):
                    # print(os.path.join(root, file))
                    im = Image.open(os.path.join(root, file))
                    w, h = im.size
                    width.append(w)
                    height.append(h)
                    if min_w + min_h > w + h:
                        min_w, min_h = w, h
                    if max_w + max_h < w + h:
                        max_w, max_h = w, h
        # JSON file for stats
        if write_into_Json:
            if os.path.isfile("statsOfDatasets.json"):
                with open("statsOfDatasets.json", "r") as read_file:
                    data = json.load(read_file)
            else:
                data = {}
        # resizing parameters for the image processing int devision because of pixels
        average_w = 0
        average_h = 0
        print("Parameters of images")
        print(len(width))
        print(len(height))
        average_w = sum(width) // len(width)
        average_h = sum(height) // len(height)
        print("Average img size: ", average_w, average_h)
        self.min_w = min_w
        self.min_h = min_h
        self.max_w = max_w
        self.max_h = max_h
        print("Min image: ", min_w, min_h)
        print("Max image: ", max_w, max_h)

        # JSON file for stats
        if write_into_Json:
            if True:
                data[len(width)] = {
                    "Average img size": (average_w, average_h),
                    "Min image": (min_w, min_h),
                    "Max image": (max_w, max_h),
                    "Number of each category": self.number_of_each_category,
                }
            else:
                data[len(width)] = {
                    "Average img size": (average_w, average_h),
                    "Min image": (min_w, min_h),
                    "Max image": (max_w, max_h),
                    "Number of each category": self.number_of_each_category,
                }

            with open("statsOfDatasets.json", "w", encoding="utf-8") as write_file:
                json.dump(data, write_file, ensure_ascii=False, indent=4)

        return average_w, average_h

    # -------------------------------------------------------------------------------
    def walk_through_data(self, path: str = "./data", ending: str = ".jpg") -> list:
        """
            doc
        """
        list = []
        for root, dirs, files in os.walk(path, topdown=False):
            for name in files:
                if name.endswith(ending):
                    list.append(os.path.join(root, name))
        return list

    def getAllLabels_(self, path: str):
        """
            all labels for classification
        """
        dir_listi = []
        for root, dirs, files in os.walk(path, topdown=False):
            if len(dirs) > 0:
                dir_listi = dirs

        return dir_listi

    def getAllImages(self, path: str, ending: str) -> list:
        """
            return all images path
        """
        list_of_files = []
        for file in os.listdir(path):
            if file.endswith(ending):
                list_of_files.append(os.path.join(path, file))

        return list_of_files

    # -------------------------------------------------------------------------------
    def open_image(self, name: str):
        """
            doc
        """
        try:
            self.img = Image.open(name)
            self.getFilename()
        except IOError:
            print("Unable to load image")
            sys.exit(1)

    def saveImage(self, img, path):
        """
            doc
        """
        img.save(path)

    def getFilename(self):
        """
            doc
        """
        # eg ./data/natural_images/person\person_0985.jpg
        self.filename = self.img.filename.split(self.path)[1].split("/")[1]
        # print(self.filename) # person_0985.jpg output

    def show(self, img=None):
        """
            doc
        """
        # print(img)
        if img == None:
            img = self.img
        img.show()

    # -------------------------------------------------------------------------------
    def cleanUpLabelStructure(self, folder):
        """
            CLEAN UP and DELETE ALL FILES AND FOLDERS
        """
        # removes also all files and subfolders
        if os.path.isdir(folder):
            rmtree(folder)
        else:
            print("No folder to delete")

    # -------------------------------------------------------------------------------
    def createFolderStructure(self, dirName, forLabeling: bool = True):
        """
            doc
        """
        if forLabeling:
            if not os.path.isdir(self.path):
                print("Label folder created")
                os.mkdir(self.path)
            if not os.path.isdir(dirName):
                os.mkdir(dirName)
        else:
            if not os.path.isdir(dirName):
                print("Folder created")
                os.mkdir(dirName)

    def quantityOfCopies(self, src, dst, level_flag: int = 1):
        """
            doc
        """
        count_second_position = 0
        # print(Manipulator.quantity_of_data_level_1)
        # print(dst)
        data_quantity = Manipulator.QUANTITY_DATA_LEVEL[level_flag - 1]
        # for _ in range(Manipulator.quantity_of_data_level_1):
        for _ in range(data_quantity):
            dst_new = dst + str(count_second_position) + ".jpg"
            copyfile(src, dst_new)
            # for new name eg => 01 02 and so on secon pos
            count_second_position += 1

    # -------------------------------------------------------------------------------
    def blurrAllImages(self, mode: bool = True):
        """
            BLURRED
        """
        index = 0
        for label in self.listOfImages:
            for file in label:
                self.open_image(file)
                self.blur_image(label=self.all_labels[index], mode=mode)
            index += 1

    def blur_image(self, name_of_file: str = "", label: str = "", mode: bool = True):
        """
            blur image
        """
        if mode:
            blur_folder = Manipulator.MANIPULATION_FOLDERNAMES[1] + label
        else:
            blur_folder = self.path + label
        if name_of_file == "":
            name_of_file = self.filename.split(".jpg")[0] + "blur_image" + ".jpg"
        blurred = self.img.filter(ImageFilter.BLUR)
        if mode:
            final_step = os.path.join(self.target_folder + blur_folder, name_of_file)
            self.saveImage(blurred, final_step)
        else:

            final_step = os.path.join(blur_folder, name_of_file)
            # print(final_step)
            self.saveImage(blurred, final_step)

    # -------------------------------------------------------------------------------
    def grayAllImages(self, mode: bool = True):
        """
            GRAY CONVERTION
        """
        index = 0
        for label in self.listOfImages:
            for file in label:
                self.open_image(file)
                self.convertImgToGray(label=self.all_labels[index], mode=mode)
            index += 1

    def convertImgToGray(self, img=None, label: str = "", mode: bool = True):
        """
            convert img to gray
        """
        if mode:
            gray_folder = Manipulator.MANIPULATION_FOLDERNAMES[0] + label
        else:
            gray_folder = self.path + label
        if img == None:
            img = self.img
        img = img.convert("L")
        img = img.convert("RGB")
        if mode:
            final_step = os.path.join(self.target_folder + gray_folder, self.filename)
        else:
            file_name = self.filename.split(".jpg")[0] + "gray" + ".jpg"
            final_step = os.path.join(gray_folder, file_name)
        self.saveImage(img, final_step)

    # -------------------------------------------------------------------------------
    def gray(self):
        """
            import cv2
            cv2.imread('filename_path')
        """
        pass

    # -------------------------------------------------------------------------------
    def rotationOfAllElements(self, mode: bool = True):
        """
            ROTATION
        """
        index = 0
        for label in self.listOfImages:
            for file in label:
                self.open_image(file)
                for count, angle in enumerate(self.rotation_angles):
                    # [15, 30, 45, 90, 180]
                    # print(count)
                    # print(angle)
                    self.rotation(
                        self.img,
                        angle,
                        count + 1,
                        label=self.all_labels[index],
                        mode=mode,
                    )
            index += 1

    def rotation(self, img, angle: int, position, label: str, mode: bool = True):
        """
            doc
        """
        if mode:
            rotation_folder = Manipulator.MANIPULATION_FOLDERNAMES[2] + label
            final_path = (
                self.target_folder + rotation_folder + "/level_" + str(position)
            )
            final_step = self.filename
        else:
            rotation_folder = self.path + label
            final_path = rotation_folder
            final_step = (
                self.filename.split(".jpg")[0] + "_level_" + str(position) + ".jpg"
            )
        rotated = img.rotate(angle)
        # final_path = self.target_folder + \
        #     rotation_folder + '/level_' + str(position)
        final_path = os.path.join(final_path, final_step)
        # print(final_path)
        # exit()
        self.saveImage(rotated, final_path)

    # -------------------------------------------------------------------------------
    def mirrorAllImages(self, mode: bool = True):
        """
            # MIRROR
        """
        index = 0
        for label in self.listOfImages:
            for file in label:
                self.open_image(file)
                self.mirroringOfImage(self.img, label=self.all_labels[index], mode=mode)
            index += 1

    def mirroringOfImage(self, img, label: str, mode: bool) -> None:
        """
            doc
        """
        if mode:
            mirroring_folder = Manipulator.MANIPULATION_FOLDERNAMES[3] + label
            final_path = self.target_folder + mirroring_folder
            filename = self.filename
        else:
            final_path = self.path + label
            filename = self.filename.split(".jpg")[0] + "mirroring" + ".jpg"
        image_obj = img
        # transpose flips the pixels
        mirroring_image = image_obj.transpose(Image.FLIP_LEFT_RIGHT)
        final_path = os.path.join(final_path, filename)
        self.saveImage(mirroring_image, final_path)

    # -------------------------------------------------------------------------------
    def cropAllImages(self, mode: bool = True):
        """
            # CROP
        """
        index = 0
        for label in self.listOfImages:
            for file in label:
                self.open_image(file)
                self.croppingImage(self.img, label=self.all_labels[index], mode=mode)
                self.croppingImage(
                    self.img, False, label=self.all_labels[index], mode=mode
                )
                self.croppingImage(
                    self.img, True, "left", label=self.all_labels[index], mode=mode
                )
                self.croppingImage(
                    self.img, False, "left", label=self.all_labels[index], mode=mode
                )
                self.croppingImage(
                    self.img, True, "right", label=self.all_labels[index], mode=mode
                )
                self.croppingImage(
                    self.img, False, "right", label=self.all_labels[index], mode=mode
                )
            index += 1

    def croppingImage(
        self,
        img,
        flag: bool = True,
        method="center",
        label: str = "",
        mode: bool = True,
    ):
        """
            center 10 to 20 percent
            flag default to folder center_10
        """
        if mode:
            cropping_folder = Manipulator.MANIPULATION_FOLDERNAMES[4] + label
        else:
            cropping_folder = self.path + label + "/"

        w, h = img.size
        square = False
        if w == h:
            reduction = self.cropGeneralizableSquare(w, flag)
            # TODO better architecture
            reduction_for_w = reduction
            reduction_for_h = reduction_for_w
            square = True
        else:
            # TODO better architecture
            reduction_for_w = self.cropGeneralizableSquare(w, flag)
            reduction_for_h = self.cropGeneralizableSquare(h, flag)
            # self.cropGeneralizable()

        # with Flag
        # The crop() method takes a 4-tuple defining
        # the left, upper, right, and lower pixel coordinates.
        # Unnecessary TODO cleaning up
        if square and method == "center":
            left = 0 + reduction
            upper = 0 + reduction
            lower = w - reduction
            right = w - reduction
        else:
            # upper corner and lower corner will not be modified
            if method == "center":
                left = 0 + reduction_for_w
                upper = 0 + reduction_for_h
                lower = h - reduction_for_h
                right = w - reduction_for_w
            else:
                upper = 0
                lower = h

            if method == "right":
                left = 0 + reduction_for_w
                right = w
            elif method == "left":
                left = 0
                right = w - reduction_for_w

        # left, top, right, bottom
        croppi_img = img.crop((left, upper, right, lower))

        if mode:
            if method == "center":
                path = os.path.join(self.target_folder + cropping_folder, "center")
                if flag:
                    path = os.path.join(path, "10")
                else:
                    path = os.path.join(path, "20")
            elif method == "left":
                path = os.path.join(self.target_folder + cropping_folder, "left")
                if flag:
                    path = os.path.join(path, "10")
                else:
                    path = os.path.join(path, "20")
            else:
                path = os.path.join(self.target_folder + cropping_folder, "right")
                if flag:
                    path = os.path.join(path, "10")
                else:
                    path = os.path.join(path, "20")

        if mode:
            final_path = os.path.join(path, self.filename)
        else:
            final_path = cropping_folder + self.filename.split(".jpg")[0] + method
            if flag:
                final_path += "10"
            else:
                final_path += "20"
            final_path += ".jpg"

        self.saveImage(croppi_img, final_path)

    def cropGeneralizableSquare(self, a: int, flag_10_percent: bool = True):
        """
            doc
        """
        self.crop_reduction_10_percent = a / 10
        self.crop_reduction_20_percent = a / 5

        # int division
        reduction = a // 10 if flag_10_percent else a // 5

        return reduction

    # -------------------------------------------------------------------------------
    def contrastAllImages(self, mode: bool = True):
        """
            contrastAllImages
        """
        index = 0
        for label in self.listOfImages:
            for file in label:
                self.open_image(file)
                self.enhanceImage(label=self.all_labels[index])
            index += 1

    def enhanceImage(self, img):
        pass

    # -------------------------------------------------------------------------------
    def creatAllManipulations(self, mode=True):
        """
            all manipulation
        """
        self.blurrAllImages(mode=mode)
        self.grayAllImages(mode=mode)
        self.rotationOfAllElements(mode=mode)
        self.mirrorAllImages(mode=mode)
        self.cropAllImages(mode=mode)

    # -------------------------------------------------------------------------------
    def extractDataFromZip(self, zip_path="./zip/*.zip", target="./data/"):
        """
            zipping file
        """
        print(zip_path)
        zf = ZipFile(zip_path, "r")
        zf.extractall(target)
        zf.close()

    def sortingDataSets(self):
        """
            sorting
        """
        # self.fifty_percent_of_data_img_path
        # self.thirty_percent_of_data_img_path
        # self.ten_percent_of_data_img_path

        # './data/maninpulated_natural_images/'
        # self.target_folder
        pass


if __name__ == "__main__":
    # mani = Manipulator(fromScratch=False, cleanUpOnly=True)
    # mani = Manipulator(fromScratch=False)
    mani = Manipulator("natural_images/")
    # mani.extractDataFromZip()
    # print(mani.listOfImages)
    # print(len(mani.listOfImages))
    # print(len(mani.walk_through_data(mani.blurred_path)))
    ####################################################################################
    # mani.creatAllManipulations(False)
    ####################################################################################
    # mani.blurrAllImages(mode=False)
    # mani.rotationOfAllElements(mode=False)
    # mani.mirrorAllImages(mode=False)
    # mani.cropAllImages(mode=False)
    # mani.grayAllImages(mode=False)
    ####################################################################################

    # mani.labelingPurpose()
    # mani.createFolderStructure(folder)

    ####################################################################################
    # mani = Manipulator('data/dogs-cats-images.zip/dog vs cat/dataset/training_set')
    # exit()
    # mani = Manipulator('dogs-cats-images', fromScratch=True)
    # zf = ZipFile('data/dogs-cats-images.zip/dog vs cat.zip', 'r')
    # zf.extractall('data/dogs-cats-images.zip/dog vs cat')
    # zf.close()
